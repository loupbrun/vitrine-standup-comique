---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}

draft: true
---

## 1. Présentation de l’artiste (bio, parcours, etc.) : 400 mots



## 2. Présentation du numéro : 100 mots



## 3. Analyse du numéro : 600 mots



### Remarques générales sur la l’espace, la scénographie, etc.



### Thématique



### Structure



### Procédés



## 4. Remarques générales / conclusion : 100 mots



## 5. Pour en savoir plus (renvoi vers des références électroniques ou autres)
