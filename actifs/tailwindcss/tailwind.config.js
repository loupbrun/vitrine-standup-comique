// TailwindCSS config file
// https://tailwindcss.com/docs/configuration
const plugin = require('tailwindcss/plugin')
module.exports = {
  theme: {

    // Hard replace
    borderWidth: {
      default: '3px',
      '0': '0',
      '2': '2px',
      '4': '4px',
      '8': '8px',
      '16': '16px',
      '24': '24px',
    },

    // Extend
    extend: {
      fontFamily: {
        display: ['triade-web', 'Knockout', 'sans-serif'],
        body: ['relato-web', 'Jenson', 'Garamond', 'Palatino', 'serif'],
      },
      colors: {
        moutarde: '#DFB317', // jaune adouci
        papier:   '#E7E3D3', // beige
        cafe:     '#87734e',  // beige foncé
        velours:  '#25481F', // vert
        blanc:    '#ffffff',
        noir:     '#000000',
      },
      screens: {
        'xl': '80rem', // 1280px
        'xxl': '90rem', // 1440px
      }
    }
  },
  variants: {
    'appearance': ['js', 'no-js', 'responsive'],
    'cursor': ['js', 'no-js', 'responsive'],
    'display': ['responsive', 'js', 'no-js'],
    'position': ['responsive', 'js', 'no-js'],
  },
  plugins: [
    plugin(function({ addVariant, e }) {
      addVariant('js', ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => {
          return `.js .${e(`js${separator}${className}`)}`
        })
      })
    }),
    plugin(function({ addVariant, e }) {
      addVariant('no-js', ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => {
          return `.no-js .${e(`no-js${separator}${className}`)}`
        })
      })
    }),
  ]
}
