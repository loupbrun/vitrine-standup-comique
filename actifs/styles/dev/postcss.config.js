const projectRoot = __dirname + '/../../../';

module.exports = {
  plugins: [
    require('postcss-import')({
      path: [projectRoot]
    }),
    // TailwindCSS
    require('tailwindcss')(projectRoot + 'actifs/tailwindcss/tailwind.config.js'),
    // Autoprefixer
    require('autoprefixer'),
    
    // Don’t run purgecss in dev
  ]
}
