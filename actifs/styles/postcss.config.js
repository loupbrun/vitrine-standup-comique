const projectRoot = __dirname + '/../../';

module.exports = {
  plugins: [
    require('postcss-import')({
      path: [projectRoot]
    }),
    require('tailwindcss')(projectRoot + 'actifs/tailwindcss/tailwind.config.js'),
    // Configuration of purgecss for Tailwindcss
    // see https://tailwindcss.com/docs/controlling-file-size/#setting-up-purgecss
    require('@fullhuman/postcss-purgecss')({
      // Specify the paths to all of the template files in your project 
      content: [
            projectRoot + 'disposition/**/*.html',
            projectRoot + 'contenus/**/*.{md,html}',
        ],
      whitelist: ['mark', 'li', 'li::before'],
      // Include any special characters you're using in this regular expression
      defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || [],
      fontFace: true
    }),
    require('autoprefixer')({
      grid: true
    }),
    require('postcss-reporter'),
  ]
}
