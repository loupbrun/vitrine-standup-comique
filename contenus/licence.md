---
title: Mentions légales
---

Sauf mention contraire, les textes sur ce site sont placés sous licence Creative Commons — Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International (CC BY-NC-SA 4.0).

## Cette licence vous autorise à :

- **Partager** — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats.
- **Adapter** — remixer, transformer et créer à partir du matériel.

## Selon les conditions suivantes :

- **Attribution** — Vous devez créditer l'Œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'Oeuvre. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l'Offrant vous soutient ou soutient la façon dont vous avez utilisé son Oeuvre.
- **Pas d’Utilisation Commerciale** — Vous n'êtes pas autorisé à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
- **Partage dans les Mêmes Conditions** — Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir du matériel composant l'Oeuvre originale, vous devez diffuser l'Oeuvre modifiée dans les même conditions, c'est à dire avec la même licence avec laquelle l'Oeuvre originale a été diffusée. 

Vous pouvez lire le [texte intégral](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr) de la licence sur le site de l’organisation Creative Commons.