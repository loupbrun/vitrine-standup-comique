---
title: L’humour, ça s’analyse!

boutons:
- texte: Explorer les analyses&nbsp;&nbsp;&rarr;
  url: /analyses

image:
  src: /img/micro.svg
  alt: Micro
---

Portraits et analyses sur le stand-up comique au Québec.