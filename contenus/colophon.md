---
title: Colophon
description: Les petits caractères.
---

Polices de caractère <a href="https://coppersandbrasses.com/retail-fonts/triade/" target="_blank" class="font-display salt">Triade</a> par Étienne Aubert Bonn et <a href="https://emtype.net/fonts/relato-serif" target="_blank" class="font-body">Relato</a> par Eduardo Manso.

- Chef de projet : [Antoine Fauchié](https://www.quaternum.net) 
- Design et développement web: [Louis-Olivier Brassard](https://loupbrun.ca)

---

Université de Montréal, mai 2020.
