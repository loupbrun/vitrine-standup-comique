---
title: À propos
description: Un mot sur ce projet.
---

Projet découlant du séminaire sur le Stand-up donné par Jean-Marc Larrue à l’Université de Montréal à l’hiver 2020.

- [Mentions légales](/licence)
- [Colophon](/colophon)
