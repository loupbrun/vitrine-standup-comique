---
title: "Simon Gouache (1)"
date: 2020-05-06
author: "Sandrine Comeau"
youtube_id: i1dCiStBoQM
duree: 1-445
description: "Sous une tonne d'autodérision, un humour intelligent."
draft: true
portrait: /img/artistes/simon-gouache.svg
---
## Présentation de l'artiste

<span class="lettrine">D</span>iplômé de l'École nationale de l'humour en 2007, Simon Gouache entreprend une carrière stimulante dans le domaine de la publicité. Bien qu'il apprécie ses conditions de travail, il ne peut ignorer l'appel irrésistible de la scène. C'est en 2009 qu'il cède et se lance dans ce qui le passionne : le stand-up comique.

De 2012 à 2015, il a l'honneur de faire la première partie du spectacle *Les heures verticales* du grand Louis-José Houde. Cette période lui offre une visibilité sans précédent. Les spectateurs apprécient son humour et sont plus de 65 000 à se procurer des billets pour son premier one-man show *Gouache* (2017).

L'humoriste trentenaire n'est pas très présent sur les réseaux sociaux et ne bénéficie pas de publicités sur les canaux traditionnels. C'est grâce au bouche-à-oreille, aux galas et à ses premières parties qu'il se taille une place dans une industrie où l'offre est de plus en plus grande.

Contrairement à la plupart de ses collègues, son amour pour la scène est exclusif : les capsules web, les émissions de télévision et les films ne l'intéressent pas.

Derrière cette apparence d'homme confiant se cache une âme sensible qui lutte quotidiennement contre des problèmes d'anxiété. Il s'inspire d'ailleurs de sa vie personnelle pour son deuxième spectacle *Une belle soirée*. Il croise les représentations de ce dernier et les premières parties de *Préfère novembre* de Louis-José Houde.

Simon Gouache travaille très fort et participe à de nombreux galas dans lesquels il ne passe pas inaperçu. Il mérite amplement les honneurs remportés au cours des dernières années soit le Prix *Victor révélation juste pour rire* (2014), Prix *Victor coup de cœur festival* (2015), *l'Olivier du numéro de l'année* (2018) ainsi que des nominations pour *l'Olivier de la découverte de l'année* (2016-2017) et *l'Olivier du
meilleur spectacle* (2019).

Alors que la plupart des humoristes écrivent leur texte pour mieux le structurer et le mémoriser, Simon Gouache assure en entrevue qu'il ne ressent pas ce besoin : « j'ai un don de mémoire et de synthèse dans ma tête, qui vient un peu de la maladie mentale, pour être bien honnête. C'est un peu un TOC »[^1].

Il tient à rejoindre un large public. Même s'il reste dans la version classique du stand-up comique, « sa façon d'incarner un gars ordinaire, sans se présenter en gros demeuré comme plusieurs de ses collègues, témoigne aussi d'une grande adresse »[^2].

De plus, il est important de souligner que « se contenter des outils classiques du stand-up exige de parfaitement les maîtriser, une qualité que Gouache peut fièrement revendiquer »[^3].

{{< bloc >}}
En se ridiculisant ainsi, il permet aux spectateurs de se sentir supérieurs et de vivre un « heureux et soudain « moment de gloire ».
{{< /bloc >}}

## Présentation du numéro

Simon Gouache s'est fait connaître en partie grâce à son numéro *Le CrossFit* qui lui a mérité *l'Olivier du numéro de l'année* en 2018.

La mise en ligne de cette performance du *Gala Juste pour rire 2017* a été vue plus de deux millions de vues sur *YouTube*, nombre significatif qui lui a clairement valu la vente considérable de billets. Simon Gouache aborde en premier lieu les Jeux olympiques et, dans un humour poli, révèle son admiration pour les athlètes. Ensuite, il relate sa première expérience dans un gym de CrossFit et renforce les idées préconçues sur ce sport, tout en adoptant un point de vue autodérisoire.

## Analyse du numéro

### Remarques générales sur la scénographie et le jeu

Comme dans tout numéro de stand-up classique, la scénographie est assez minimaliste. Un système d'éclairage particulier a été installé pour le gala, sinon, seuls l'humoriste, le micro et le pied du micro occupent l'espace scénique. L'artiste réussit tout de même à personnaliser l'utilisation qu'il en fait.

Dans les premières secondes, l'humoriste {{< temp 15 >}} empoigne le micro et déplace le pied, comme s'il n'en avait plus besoin. Tous les éléments s'avèrent finalement utiles : le fil du micro devient une corde qu'il agite à de nombreuses reprises pour illustrer un exercice de torture {{< temp 240 >}} et le pied du micro se transforme en poteau sur lequel s'appuie l'entraîneur lorsqu'il craque des noix avec ses fesses {{< temp 293 >}}. Simon Gouache exploite l'espace scénique (l'espace de la scène tel que le voit le public) pour mieux illustrer l'espace dramatique (le lieu dans lequel se déroule l'histoire). Il bouge sur scène comme le font les personnages sportifs qu'il incarne. Ces déplacements contribuent largement à clarifier les images évoquées.

L'humoriste semble très à l'aise sur scène. Ses vêtements sont assez classiques et pourraient passer pour une tenue de tous les jours, appuyant l'impression de vérité et d'authenticité[^4].

### Thématique et procédés

Simon Gouache aborde de manière originale une thématique récurrente dans le stand-up : le sport. Il imagine des Jeux olympiques où les gens normaux sont invités à performer. Les spectateurs n'ont pas besoin d'avoir des connaissances précises sur les disciplines, ils peuvent facilement s'identifier aux propos de l'humoriste.

Après s'être moqué du quinquagénaire bedonnant forcé à courir un marathon, il raconte sa première séance peu glorifiante de *CrossFit*. Ce sport est un sujet atypique, car il a gagné en popularité seulement dans les dernières années. Alors qu'il peut être difficile de parodier le hockey sans tomber dans les clichés, le jeune homme avait devant lui toutes les possibilités. Pour des raisons obscures, le CrossFit entretient une image de hobby inaccessible pour la population générale et l'humoriste choisit d'accentuer ce préjugé. Pour ce faire, il utilise abondamment le procédé de l'exagération tant dans la description des athlètes et des exercices que celle de ses faiblesses

Simon Gouache obéit à une recommandation bien connue du monde de l'humour : donner l'impression que l'anecdote lui est réellement arrivée. Cependant, même si tout semble sincère, nous savons très bien qu'il exagère ses propos pour mettre les sportifs sur un piédestal. Il décrit les trois hommes qui s'entraînent comme s'ils étaient cinq fois plus en forme que lui, mauves et hurlant de douleur {{< temp 234 >}}. Il brosse un portrait flatteur du coach en insistant sur son jeune âge, ses mollets en marbre, sa capacité à courir six marathons par année et à briser des noix de Grenoble avec ses muscles fessiers {{< temp 284 >}}.

L'humoriste a eu l'idée d'exagérer la médiocrité de sa condition physique, créant un écart encore plus prononcé entre les autres et lui. Par exemple, il avoue sans gêne que sa tête tourne s'il se lève trop vite, que son bras le fait souffrir lorsqu'il se brosse les dents {{< temp 270 >}}, qu'il a vingt livres en trop et que trois *Coors Light* constituent son dîner {{< temp 310 >}}. Ce procédé permet d'imager clairement la comparaison avec les autres et accentuer les effets d'autodérision.

Aussi, Simon Gouache nous démontre sa compréhension d'un principe clé de l'humour : « l'humour naît de ce qu'une personne éprouve un sentiment de supériorité devant une autre »[^5]. En se ridiculisant ainsi, il permet aux spectateurs de se sentir supérieurs et de vivre un « heureux et soudain « moment de gloire ». Le rire ou le sourire est l'expression de cette satisfaction ressentie devant les carences, les travers et les défauts que l'on constate chez autrui »[^6].

## Conclusion

En conclusion, il est difficile de résumer l'étendue du talent de Simon Gouache en une capsule. Son humour intelligent renferme une complexité dont on ne pourrait se douter tellement tout se fait naturellement sur scène. Il adopte un point de vue autodérisoire et s'il rit d'une personne, c'est souvent pour montrer son admiration pour celle-ci. Ce prodige du stand-up comique a pris plus de temps pour se faire connaître du grand public, mais ces années de réflexion sur sa pratique auront servi à tendre la corde de l'arc qui le propulsera au sommet de son art.

## Pour en savoir plus

### Bibliographie

{{< references >}}
BAILLARGEON, Normand et Christian BOISSINOT, *Je pense, donc je ris*,
Québec, Presses de l\'Université Laval, Coll. « Quand la philo fait
pop!» 2010, 258 p.

BERGERON, Steve, « La scène avant tout pour Simon Gouache », *La
tribune*, 12 juin 2018,
<https://www.latribune.ca/arts/la-scene-avant-tout-pour-simon-gouache-92d3f1a5eeca40e552853baa2215ea1f>¸

GODIN, Sandra, « Un succès mérité », *Le journal de Québec,* 2 décembre
2017, <https://www.journaldequebec.com/2017/12/02/un-succes-merite>

LAUZON, Véronique, « Le retour musclé de Simon Gouache », *La Presse*, 7
octobre 2019,
<https://www.lapresse.ca/arts/humour/201910/06/01-5244323-le-retour-muscle-de-simon-gouache.php>

LARRUE, Jean-Marc, « Composantes et procédés du stand-up », Montréal :
Université de Montréal, session hiver 2020.

MORASSE, Catherine, « Simon Gouache: rien que pour le plaisir », *Le
droit,* 2 juillet 2019,
<https://www.ledroit.com/arts/spectacles-et-theatre/simon-gouache-rien-que-pour-le-plaisir-113d20e1c95db7a815fbb7897cf1a1bb>

OUELLET, Sylvie et Christiane VIEN, *Écrire l'humour, c'est pas des
farces*, Montréal, Éditions Druides, 2017, 240 p.

PICOTTE, Vanessa, «Simon Gouache: portrait d'un humoriste qui ne vit que
pour la scène», *Le courrier du Sud*, 20 janvier 2020,
<https://www.lecourrierdusud.ca/simon-gouache-portrait-dun-humoriste-qui-ne-vit-que-pour-la-scene/>

TARDIF, Dominique, «Une belle soirée»: l'humour de Simon Gouache, de
l'art ou pas?», *Le Devoir*, 10 octobre 2019,
<https://www.ledevoir.com/culture/theatre/564428/critique-humour-l-humour-de-simon-gouache-de-l-art-ou-pas>

Sans auteur. « Simon Gouache », sur le site de *Phaneuf*. Consulté le 24
mars 2020. <https://phaneuf.ca/artistes/simon-gouache/>
{{< /references >}}

[^1]: GODIN, Sandra, « Un succès mérité », *Le journal de Québec*, 2
    décembre 2017,
    https://www.journaldequebec.com/2017/12/02/un-succes-merite

[^2]: TARDIF, Dominique, « «Une belle soirée»: l'humour de Simon
    Gouache, de l'art ou pas?», *Le Devoir*, 10 octobre 2019,
    <https://www.ledevoir.com/culture/theatre/564428/critique-humour-l-humour-de-simon-gouache-de-l-art-ou-pas>

[^3]: TARDIF, Dominique, « «Une belle soirée»: l'humour de Simon
    Gouache, de l'art ou pas?», *Le Devoir*, 10 octobre 2019,
    <https://www.ledevoir.com/culture/theatre/564428/critique-humour-l-humour-de-simon-gouache-de-l-art-ou-pas>

[^4]: LARRUE, Jean-Marc, « Composantes et procédés du stand-up»,
    Montréal : Université de Montréal, session hiver 2020.

[^5]: BAILLARGEON, Normand et Christian BOISSINOT, *Je pense, donc je
    ris*, Québec, Presses de l'Université Laval*,* Coll. «Quand la
    philo fait pop!» 2010, 258 p.

[^6]: BAILLARGEON, Normand et Christian BOISSINOT, *Je pense, donc je
    ris*, Québec, Presses de l'Université Laval, Coll. «Quand la philo
    fait pop!» 2010, 258 p.
