---
title: "Eddy King"
date: 2020-05-06
author: "Aude Jay"
youtube_id: E1IihzMTCHU
description: "La recette de l'humoriste Eddy King ? Dérision, divertissement et réflexion."
draft: true
portrait: /img/artistes/eddy-king.svg
---

## Présentation de l'artiste
<span class="lettrine">H</span>umoriste franco-québécois d'origine congolaise, Eddy King est né en France le 14 décembre 1982, à Goussainville, en banlieue parisienne. C'est en 1995, à l'âge de 13 ans, qu'il arrive au Québec avec sa mère et qu'il s'installe à Montréal : attiré par les arts de la scène, il commence à écrire des textes de rap et fonde le groupe Dögone Tribe cinq ans plus tard, avec lequel il remporte en 2004 le premier prix du Festival Hip Hop 4ever à Montréal. Il finit cependant par se tourner vers le stand-up en participant tout d'abord à des soirées dans des bars, aussi bien en français qu'en anglais, lors desquelles il parvient à se faire connaître. En 2007, il est sacré « Coup de cœur » au concours de la relève du festival *Juste pour rire* et termine finaliste du concours *En route vers mon premier gala* en 2008. Un mois plus tard, il fait les premières parties du spectacle de l'humoriste Rachid Badouri *Arrête ton cinéma*, ce qui lui permet de bénéficier d'une large médiatisation, et il lance son premier one-man-show en 2010. Outre ses participations aux galas *Juste pour rire* et ses passages au *Bordel Comédie Club*, il apparaît également à la radio, dans des émissions télévisées et au cinéma, puisqu'il joue notamment dans le film *La Chute de l'empire américain* de Denys Arcand en 2018. L'humoriste lancera prochainement son deuxième one-man-show, *Mokonzi*.

Si Eddy King cherche avant tout à faire rire et à divertir son public, l'humour est également pour lui un moyen de s'exprimer et de faire porter sa voix. Il n'hésite pas, ainsi, à aborder des sujets sensibles qui l'indignent ou le touchent directement, comme le racisme, la colonisation, la xénophobie ou le profilage racial : l'un de ses numéros les plus connus est par exemple « Tintin au Congo », où il met en lumière les stéréotypes racistes sur lesquels repose la célèbre bande dessinée d'Hergé. À partir de ses expériences et de ses convictions, il cherche donc à amener les spectateurs à se poser des questions et à leur faire prendre conscience de certaines réalités dont ils ne s'apercevaient pas forcément.

## Présentation du numéro
« Le camping » est un sketch filmé à l'occasion du festival *Juste pour rire*. Dans ce numéro, Eddy King joue sur son statut d'immigrant et sur les différences culturelles qui le séparent des Québécois pour poser un regard neuf sur un loisir québécois, le camping, et, plus largement, le goût pour la nature. Mêlant autodérision et anecdotes personnelles tout au long du sketch, l'humoriste tourne à la fois en dérision cette activité québécoise et ses propres réactions face à celle-ci.

{{< bloc >}}
sa force repose sur la transposition inattendue qui fait basculer du sujet (inoffensif) du chalet dans les bois au thème de l’esclavage.
{{< /bloc >}}

## Analyse du numéro

### Espace, jeu et scénographie
La scène sur laquelle performe Eddy King est relativement nue, à l'exception d'un fond constitué de photographies d'arbres où prédomine la couleur verte, qui ne peut que rappeler le thème du numéro. L'humoriste a pour seul accessoire un micro qu'il tient à la main, ce qui le laisse relativement libre de ses mouvements et lui permet d'occuper efficacement l'espace : il rythme et appuie ses paroles par de nombreux gestes, et fait même mine de quitter la scène pour illustrer son départ du chalet de sa productrice. L'importance de la gestualité et des déplacements lui donne ainsi l'occasion de souligner, d'accentuer ou de mimer ses propos, et de donner plus de poids à son récit tout en feignant l'incrédulité, la frayeur ou encore la stupéfaction. Au cours du numéro, Eddy King incarne par ailleurs quelques personnages dramatiques : son professeur d'éducation physique de Cégep et sa productrice Véro pour les principaux, mais aussi ses « ancêtres congolais » et les animaux de la forêt. Pour jouer ces différents personnages, il continue à s'appuyer sur ses mouvements (pour imiter les animaux, par exemple) mais fait également appel à sa voix, notamment pour contrefaire les accents congolais et québécois. Tout cela, en plus de contenir une forte charge comique, lui permet donc de mener son récit avec efficacité et énergie.

### Thématique
Le sketch, qui s'articule autour du thème du séjour en forêt (que ce soit sous une tente ou dans un chalet), repose sur l'expérience personnelle de l'humoriste : il est en effet bâti sur le choc culturel ressenti par Eddy King lors de son arrivée à Montréal à propos du rapport des Québécois à la nature. Le numéro se construit ainsi sur l'opposition entre les habitudes québécoises et l'attitude du performeur, ce qui entraîne le rire. Cependant, si la plus grande partie du numéro se fonde sur des éléments personnels ou culturels ancrés dans la situation contemporaine, il s'ouvre pour finir sur une donnée historique : l'esclavage des Africains envoyés « cueillir du coton » en Amérique. Le numéro acquiert donc finalement une portée qui va au-delà de la simple anecdote, en suscitant une réflexion plus profonde : il ne s'agit plus seulement d'une comparaison culturelle relative au camping mais d'un rappel de l'esclavage qui, à l'époque où les colons européens vivaient « au fond des bois », asservissait sur le continent américain les populations arrachées d'Afrique.

### Structure
Le numéro s'ouvre sur une constatation générale de l'humoriste : le « sens de l'aventure » des Québécois, qu'il met en relation avec leur goût pour les activités en forêt et qu'il oppose à son propre rapport à la nature. Eddy King illustre ensuite ce constat par deux exemples tirés d'anecdotes personnelles (le camping avec sa classe de Cégep et le chalet de Véro), au cours desquels il ne cesse de faire alterner monologue, dialogues fictifs et interaction avec le public : son récit est en effet entrecoupé de simulations de dialogues entre son propre persona et son professeur puis sa productrice, et il prend fréquemment à témoin les spectateurs (« j'vous ai dit *des ours*, vous m'avez tous regardé comme : ben oui Eddy, des ours », « vous voyez vous réagissez même pas quand j'vous dis ça », « quoi *OH*, vous avez l'air déçus d'la fin d'l'histoire ? »). Le public, en tant que représentant des Québécois, devient ainsi un personnage à part entière du numéro, puisqu'il permet à l'humoriste de se positionner en contraste avec les réactions de celui-ci. À travers ces interactions, Eddy King tisse en outre une véritable connivence avec les spectateurs qui, interpellés et invités à réagir, deviennent partie prenante du numéro.

### Procédés
De manière générale, les punchs livrés au cours du numéro sont basés sur l'exagération (de la peur de l'humoriste, comme lorsque ce dernier affirme que s'il « marche sur l'trottoir et ... voi[t] un écureuil qui vient vers [lui], [il] change de trottoir ») et les différences de points de vue entre les Québécois et lui. Tout au long du numéro, King multiplie les renversements entre la conception usuelle du camping et sa propre vision de cette activité : dormir sous une tente devient « jouer au sans-abri dans les bois », par exemple, et effrayer les ours avec une cloche consiste finalement à leur indiquer « l'heure du lunch ». Si le dernier punch du numéro tient lui aussi de cette logique du renversement de point de vue, il est cependant également d'un autre ordre, au niveau de la portée comme de la puissance : introduit par une prémisse beaucoup plus longue que lors des gags précédents et dite très lentement, avec une lenteur qui contraste avec le reste du sketch, sa force repose sur la transposition inattendue qui fait basculer du sujet (inoffensif) du chalet dans les bois au thème de l'esclavage. À travers l'implicite historique que suggère la « cueill[ette] d[e] coton », Eddy King nous transporte donc finalement dans une dimension politique que ne laissait guère présager le reste de son numéro.

## Conclusion

{{< bloc >}}
Le numéro acquiert donc finalement une portée qui va au-delà de la simple anecdote, en suscitant une réflexion plus profonde
{{< /bloc >}}

Ainsi, si la majeure partie du sketch tourne à la fois en dérision les craintes de l'humoriste et les habitudes québécoises, la puissance de la chute conclusive permet de doter le numéro d'une profondeur historique et politique qui n'est pas sans contraster avec la légèreté du thème général. Tout en proposant un humour ancré dans des situations de la vie quotidienne (l'éducation physique au Cégep, la traversée de Montréal Nord en voiture, les chalets en forêt, etc.), Eddy King finit par aborder un sujet beaucoup plus grave : évoquer l'esclavage lui permet en effet de pousser son public à réfléchir à la condition qui, à l'époque, était celle des Afro-descendants en Amérique. Se mêlent ainsi divertissement et réflexion, dans un lien de complicité continu avec les spectateurs.

## Pour en savoir plus

- Site Internet d'Eddy King : [https://www.eddyking.net](https://www.eddyking.net)
- Podcast de Guillaume Wagner, « Eddy King » : [https://www.youtube.com/watch?v=Kg5dYvEzcfA](https://www.youtube.com/watch?v=Kg5dYvEzcfA)
- Articles de presse :

{{< references >}}
AIT EL MACHKOURI Zora, « Eddy King », *Jeune Afrique*, 5 mars 2010 : <https://www.jeuneafrique.com/198163/culture/eddy-king/>

CLÉMENT Éric, « Le profilage racial vu par Eddy King : mieux vaut en rire\... », *La Presse*, 28 mai 2010 : <https://www.lapresse.ca/arts/spectacles-et-theatre/humour-et-varietes/201005/28/01-4284570-le-profilage-racial-vu-par-eddy-king-mieux-vaut-en-rire.php>

VALLET Stéphanie, « Sur le divan avec Eddy King », *La Presse*, 8 octobre 2011 : <https://www.lapresse.ca/arts/spectacles-et-theatre/humour-et-varietes/201110/08/01-4455567-sur-le-divan-avec-eddy-king.php>
{{< /references >}}
