---
title: "Philippe-Audrey Larrue-St-Jacques"
date: 2020-05-06
author: "Émilie Coulombe"
youtube_id: zi5UV8KXw70
description: "Un artiste rafraîchissant et virtuose."
draft: true
portrait: /img/artistes/philippe-audrey-larrue-st-jacques.svg
---

## Présentation de l'artiste
<span class="lettrine">N</span>é en 1987 dans une famille d’intellectuels, Philippe-Audrey Larrue-St-Jacques est un artiste dont la place n’est plus à faire. Fort d’une double formation professionnelle de comédien (Conservatoire d’art dramatique, 2007-2010) et d’humoriste (École nationale de l’humour, 2012-2014), le polyvalent trentenaire se présente comme un gentleman tablant sur sa vaste culture ainsi que ses goûts décalés pour les jardins français et la poésie.

Larrue-St-Jacques se distingue en effet par le regard critique qu’il porte tant sur le monde qui l’entoure que sur la culture du rire à laquelle il participe. Il a parcouru le Québec, une partie du Canada, mais aussi diverses régions de la Suisse et de la France pour partager son humour dans les salles du circuit professionnel comme dans les bars et les _comedy clubs_. Sa carrière connaît une ascension fulgurante à partir de sa participation à la série _Like-moi!_ en 2016 qui, en plus de le faire connaître du grand public, lui vaut le Gémeau 2018 de la meilleure interprétation humoristique.
Depuis _Like-moi!_, on a entre autres pu le voir jouer dans la série Web _Téodore pas de H_ (2018), l’émission de télévision _Léo_ (2018) et, au cinéma, dans _De père en flic 2_ (2017). En 2019, il décroche le premier rôle du film indépendant _Les barbares de la Malbaie_ (2019), dans lequel il campe un ancien joueur de la Ligue nationale de hockey.

{{< bloc >}}
« Je vais être très honnête avec vous, St-Denis, je me trouve laid ».
{{< /bloc >}}

L’angle original avec lequel il appréhende la société et la culture fait aussi de lui un chroniqueur en demande. À ce titre, il a notamment collaboré à des productions télévisuelles et radiophoniques comme _Code G_ (2016-2017), _Plus on est de fous, plus on lit_ (2017-2018), _On est tous debout_ (2018), _Le clan MacLeod_ (2018-2019), _On dira ce qu’on voudra_, _Esprit critique_, _La soirée est (encore) jeune_ et _Tout un matin_. On a également pu le lire dans _Urbania_ (2015-2018) et _La Presse +_.

_Hélas, ce n’est qu’un spectacle d’humour_, premier one-man-show de l’artiste, rencontre un vif succès dès sa représentation inaugurale au Zoofest 2017, lors duquel Larrue-St-Jacques remporte d’ailleurs le prix Coup de cœur du festival. Porté par cette victoire et de nombreuses critiques élogieuses, l’humoriste continue de présenter ce spectacle à travers le Québec, tout en préparant progressivement le deuxième. Son achèvement pourrait attendre, cela dit, puisque Larrue-St-Jacques approche l’écriture humoristique comme un « art de la verve et du style » qui demande temps, « précision linguistique[^1] » ainsi qu’attention méticuleuse aux rythmes et aux sonorités.

## Présentation du numéro
Présenté le 8 mai 2018 au Théâtre St-Denis dans le cadre du gala-bénéfice célébrant les 30 ans de l’École nationale de l’humour, « Le nain » porte sur quelques déboires de Larrue-St-Jacques. En six minutes, l’humoriste raconte comment il s’est senti à l’écart tant au secondaire que pendant ses études en théâtre, lors desquelles il a, entre autres, dû interpréter le rôle d’un nain hydrocéphale. Cohérent avec sa pratique générale du stand-up, où brillent les thématiques de la déception et de l’échec, le numéro à l’étude dépeint un homme qui dit ne pas plaire, mais qui sait admirablement rire de ses faiblesses.


## Analyse du numéro

### Remarques générales sur l’espace
Performé dans un contexte festif – et donc dans un décor moins minimaliste que ce qu’on observe habituellement dans les bars et les comedy club –, « Le nain » se déploie dans un espace composé d’un  fauteuil, d’une lampe, d’un tabouret, sur lequel est posé un livre; d’un mur de fond de scène orné de photos de finissants et de projecteurs lumineux colorés; ainsi que d’un écran de projection.

Notons que le livre constitue un accessoire récurrent dans l’esthétique de Larrue-St-Jacques, qui se singularise notamment par des scènes de lecture à haute voix, que l’on pense à la lecture d’un extrait de l’Histoire de la sexualité de Foucault lors de son premier exercice public avec l’École nationale de l’humour, de poèmes de Nelligan dans Hélas, ce n’est qu’un spectacle d’humour ou encore du résumé de la pièce de théâtre Divines paroles dans le numéro à l’étude.


### Thématique
« Le nain » s’appuie sur la stratégie rhétorique de l’autodérision. Vêtu, comme à l’accoutumée, d’un complet veston-cravate, Larrue-St-Jacques se confie, micro en main, sur sa faible estime physique et partage, dans un jeu moins larmoyant que contrarié, irrité, des épisodes de sa vie où il s’est senti malvenu.

{{< bloc >}}
Il réussit à donner vie à un alter ego mondain aux formules parfois littéraires sans sacrifier la clarté ou l’apparence de conversation naturelle avec l’assistance.
{{< /bloc >}}

### Structure
Une structure tripartite s’organise à partir de la prémisse du numéro : « Je vais être très honnête avec vous, St-Denis, je me trouve laid ». Une première séquence porte sur la répulsion que les Frères administrant son école auraient éprouvée à l’idée de le « toucher »; une deuxième, sur le complexe d’infériorité qu’il a intériorisé par rapport à ses meilleurs amis du secondaire; une troisième, plus longue, sur l’épisode de sa vie où il s’est le moins senti acclimaté, c’est-à-dire au Conservatoire lors de son spectacle de finissants.

Bien que les trois épisodes reposent sur des anecdotes a priori distinctes, ils demeurent fortement teintés et reliés par la proposition initiale. C’est la laideur qui justifierait qu’aucun Frère n’ait agressé Larrue-St-Jacques. C’est cette même apparence qui expliquerait son impopularité au sein de son groupe d’âge. Le thème est finalement récupéré par l’anecdote du nain hydrocéphale hideux : « Je disais que j’étais laid, mais quand même, je n’ai pas le casting de l’hydrocéphale tant que ça » {{< temp 222 >}}.

Quant à la chute du numéro, elle surprend par son positivisme qui rompt avec la tonalité générale : à l’École nationale de l’humour, enfin, Larrue-St-Jacques a eu l’impression d’être à sa place.

### Procédés
Des mécanismes comiques viennent renforcer cette cohérence organisationnelle travaillée, en surface comme en profondeur, par la règle de trois : au secondaire, l’humoriste a été pensionnaire, servant de messe et choriste; sur scène, il présente les portraits flatteurs de ses amis Julien Hurteau et Niels Schneider, avant de montrer une photo désavantageuse de sa personne. Plusieurs gags reposent effectivement sur la juxtaposition de deux premiers éléments et d’un troisième qui vient en rompre la logique pour mieux créer la surprise et déclencher le rire :

> Pour me consommer intégralement, il y avait 3 raisons : l’ignorance, l’erreur ou « t’es pas game ». {{< temp 101 >}}
>
> […] à la fin de la formation, des études ou… de la torture. {{< temp 126 >}}
>
> Mes profs se sont dit : « Philippe-Audrey, il mesure 6 pieds 2, il pèse 197 livres (de muscles), mais on va quand même lui faire un rôle de nain ». {{< temp 192 >}}
>
> [Le masque] était le résultat d’une orgie violente entre E.T., un ballon de basket dégonflé et un Monsieur Patate laissé sur un rond de poêle. {{< temp 233 >}}

L’ironie est un autre outil auquel recourt fidèlement l’humoriste. À titre d’exemple, il lance que son ami «  Julien […] est comme dégueulasse […], pourri » {{< temp 60 >}}, que Niels est un « étron » {{< temp 71 >}} et, enfin, que le résumé de la pièce Divines paroles est « passionnant » {{< temp 274 >}}, laissant tomber au même instant le livre au sol. Dans les trois cas, il est évident que Larrue-St-Jacques entend le contraire de ce qu’il avance pour amuser l’assistance.
Reste que le moment qui déclenche le plus franchement l’hilarité est celui où les photos d’une représentation de Divines paroles sont projetées : l’anticipation du public, fondée sur les descriptions de Larrue-St-Jacques, rencontre à cette occasion la preuve concrète de l’existence du nain hydrocéphale, ce qui provoque le rire. Certes, l’humoriste se montre convaincant dès le début de l’anecdote et on devine rapidement qu’elle est avérée, sincère, mais les photos surprennent néanmoins, puisqu’on se rend compte de l’absence d’exagération : le costume de nain est, sans conteste, horrible, peut-être même plus affreux que ce qu’on a pu imaginer.

## Conclusion
Bref, Larrue-St-Jacques est un artiste rafraîchissant à la plume et au jeu sans pareils. Alors que les spécialistes soutiennent que « [l]’écriture humoristique doit être claire et concise[, qu’il n’est] [p]as question de littérature[^2] », il rivalise d’ingéniosité et réussit, « qu’à cela ne tienne » {{< temp 201 >}}, à donner vie à un alter ego mondain aux formules parfois littéraires sans sacrifier la clarté ou l’apparence de conversation naturelle avec l’assistance. Grand virtuose, il convoque du même souffle références élitistes et populaires d’un air à la fois vulnérable et en contrôle. Cette humanité est aussi précieuse que sa soif de pousser l’industrie humoristique à devenir la meilleure version d’elle-même. Larrue-St-Jacques a raison : il est, en humour, « totalement à la bonne place » {{< temp 345 >}}.


## Ressources

{{< references >}}
https://www.youtube.com/watch?v=zi5UV8KXw70

« Bio et CV », Philippe-Audrey Larrue-St-Jacques [en ligne], http://www.philippe-audrey.com/. Page consultée le 7 avril 2020.

DUMAIS, Manon (2017), « Zoofest : je choisis Philippe-Audrey Larrue-St-Jacques », Le Devoir [en ligne], https://www.ledevoir.com/culture/503212/entrevue-phil-audrey-larue-st. Page consultée le 7 avril 2020.

LARRUE-ST-JACQUES, Philippe-Audrey (2019), « Apprendre à aimer l’humour », Jeu, n° 172, p. 55-57.

LARRUE-ST-JACQUES, Philippe-Audrey (2017), « J’ai été l’élève de Gilbert Sicotte », Urbania [en ligne], https://urbania.ca/article/jai-ete-leleve-de-gilbert-sicotte/. Page consultée le 20 avril 2020.

RADIO-CANADA (2018a), « Les habits de Philippe-Audrey Larrue-St-Jacques », Christiane Charrette [en ligne], https://ici.radio-canada.ca/premiere/emissions/christiane-charette/segments/entrevue/79055/philippe-audrey-larrue-st-jacques. Page consultée le 8 avril 2020.

RADIO-CANADA (2018b), « Philippe-Audrey Larrue-St-Jacques : je lis pour savoir où, comment et quoi regarder », Radio-Canada Arts [en ligne], https://ici.radio-canada.ca/nouvelle/1098901/philippe-audrey-larrue-st-jacques-je-lis-pour-savoir-ou-comment-et-quoi-regarder. Page consultée le 20 avril 2020.

ROY, Marie-Josée (2017), « Philippe-Audrey Larrue-St-Jacques : au-delà de son nom (entrevue) », HuffPost Québec [en ligne], https://quebec.huffingtonpost.ca/2017/06/20/entrevue-philippe-audrey-larrue-st-jacques_n_17223924.html. Page consultée le 8 avril 2020.

TREMBLAY, Karine (2019), « Philippe-Audrey Larrue-St-Jacques : lire, écrire, faire rire », La Tribune [en ligne], https://www.latribune.ca/arts/philippe-audrey-larrue-st-jacques--lire-ecrire-faire-rire-7614b66242e41a6230c26c8983c74131. Page consultée le 16 avril 2020.
{{< /references >}}

[^1]: Philippe-Audrey Larrue-St-Jacques (2019), « Apprendre à aimer l’humour », Jeu, n° 172, p. 57.
[^2]: Sylvie Ouellette et Christiane Vien (2017), Écrire l’humour, c’est pas des farces !, Montréal, Druide, p. 23-24.
