---
title: "Les Pic-Bois"
date: 2020-05-06
author: "Julien Béland-Bonenfant"
youtube_id: iHtMqWlRLsY&t=387s
duree: 387-567
draft: true
description: "Les Pic-Bois prônent avant tout la liberté de rire."
portrait: /img/artistes/les-pic-bois.svg
---
## Présentation du duo
<span class="lettrine">L</span>es Pic-Bois sont un duo humoristique québécois formé par Dominic Massicotte et Maxime Gervais, originaires de Salaberry-de-Valleyfield. Leur désir commun de monter sur scène remonte au secondaire, une période durant laquelle ils commencent déjà à mettre sur pied de petits numéros sans prétention. Ils se font connaître en participant à plusieurs émissions d'*En route vers mon premier gala Juste pour rire* de 2009 à 2013, ainsi qu'à plusieurs éditions du festival Zoofest. De septembre 2017 au mois de mai 2019, le duo se lance de manière indépendante dans la production d'une série de spectacles, à raison d'un spectacle par mois. Ce travail considérable leur a attiré l'admiration du milieu de l'humour ainsi que l'attention médiatique du public.

Les Pic-Bois font-ils de l'humour absurde ? À cette question, Dominic Massicotte répond :

> Il y a beaucoup de gens qui disent qu'on fait de l'absurde, mais je pense que ce qu'on fait, c'est plus du niaiseux \[...\] Tsé, ça peut être réfléchi du niaiseux ! Je dis qu'on ne fait pas de l'absurde, parce que l'absurde, c'est surprendre avec une image inusitée. Il y a quelque chose dans l'absurde qui brise le lien avec le réalisme, mais nos personnages, eux, sont joués de façon réaliste.[^1]

Si leur humour est plus souvent qu'autrement qualifié de « niaiseux », comme le confirme Dominic Massicotte, l'humour des Pic-Bois détient bel et bien des éléments qui s'inscrivent dans la tradition de l'absurde. Les deux humoristes sont constamment à la recherche de nouveaux défis à la hauteur de leur imagination débordante.

## Présentation du numéro
Ce numéro des Pic-Bois intitulé *L'après-ski jazz* a été tourné dans le cadre de l'émission *En route vers mon premier gala Juste pour rire* en 2013. Il s'agit d'un numéro de stand-up avec liens, c'est-à-dire que les blagues du numéro sont reliées les unes aux autres suivant une logique dictée par le jeu scénique. Cependant, ces blagues n'entretiennent pas de lien direct avec la thématique annoncée au tout début du numéro. Dans l'humour absurde particulièrement, le fond du numéro (la thématique, la suite logique des évènements) est indissociable de sa forme (la manière de parler, de se mouvoir et de se représenter). Dans ce cas-ci, la thématique ne sert que de cadre aux blagues qui s'enfilent. Comme la forme du numéro provient d'une rencontre absurde, c'est-à-dire à contre-courant du sens commun, il faut s'attendre à ce que les blagues le soient tout autant.

## Analyse du numéro
Dans la majorité de leurs numéros, les Pic-Bois incarnent des personnages aux allures déjantées. La composante théâtrale est une partie importante de leurs numéros. Celui-ci ne manque pas à la règle. Les personnages arrivent sur scène en habits de neige, avec des bottes de ski aux pieds. En regardant de plus près, on remarque qu'ils sont maquillés et affublés de grosses moustaches et de sourcils. Ils ont également comme accessoire un verre de gin à la main. Reprenons la citation de Dominic Massicotte citée un peu plus haut. Si certains propos des personnages trouvent effectivement leur référent dans la réalité, il n'en reste pas moins que l'exagération poussée à l'extrême de certains de leurs attributs et de leur propos général relève d'une construction tout à fait fictive. Ces personnages ont une histoire personnelle qui ne comporte aucun élément anecdotique. Ils n'existent que l'instant du numéro dans le simple but de provoquer le rire.

La musique de jazz en arrière-fond, qui crée une atmosphère décontractée, est inattendue. En effet, les Pic-Bois utilisent tous les moyens qui leur sont accessibles pour créer un univers bien particulier. Avant même qu'ils aient commencé à parler, cet univers est déjà mis en place et suscite l'intérêt par son aspect original.

Les premières répliques du numéro informent le spectateur de sa thématique, celle de l'après-ski jazz. On comprend que les deux personnages, qu'on imagine être des amis, reviennent d'une journée de ski et qu'ils prennent maintenant un petit verre pour se reposer. Le numéro se structure comme un long échange entre les deux personnages, sans présenter de véritable conflit ou de problématique.

{{< bloc >}}
Les Pic-Bois prônent avant tout la liberté de rire, ce qui donne à leurs numéros un côté rafraîchissant et original.
{{< /bloc >}}

Tout au long du numéro, le spectateur prend conscience que la prémisse de départ n'est pas respectée. En effet, le numéro ne concerne en rien le sport de glisse ou la musique rythmée. Ces deux éléments ne sont qu'un prétexte pour susciter la rencontre absurde de deux activités opposées, par laquelle les personnages pourront se permettre de dire tout et n'importe quoi, en vertu de la liberté qu'elle confère. Le public sent dès les premiers instants l'absurdité d'une telle situation, tout à fait improbable. Il s'agit simplement d'une expérience formelle, d'un exercice de composition qui réunit deux mondes opposés. C'est donc un humour qui relève complètement de l'artificiel et de la construction, et qui rompt avec le stand-up actuellement dominant, plus lié à la réalité.

Les deux personnages abordent le piège du sextape, pour ensuite enchainer avec les comportements douteux de leurs (enfants) ados, ce qui aboutira à une conversation sur leurs triomphes sexuels. Une possible situation de conflit survient en fin de numéro. Elle n'en est cependant pas une et ne sert qu'à raffermir de façon surprenante la complicité des deux personnages. La thématique qui unit les blagues les unes aux autres est cette suite de mésaventures sexuelles qu'ont vécues les deux personnages. À mon sens, le principal lien entre chacune des blagues est davantage la liberté outrancière avec laquelle les personnages s'expriment.

Les Pic-Bois utilisent plusieurs procédés comiques. Le procédé le plus utilisé dans ce numéro est évidemment l'incompatibilité logique. Celle-ci consiste à associer des éléments qui n'ont pas l'habitude de l'être, comme dans cet extrait : « \[...\] j'ai ben de la misère avec ma fille ces temps-ci. \[...\] Elle commence à porter des j-strings... Ben elle veut faire comme son père... ». Le rire est provoqué en raison de la contradiction que cette suite d'éléments crée. S'y trouve également la règle de trois, ou plutôt de quatre, avec cet enchaînement d'éléments : « général tao », « muraille de chine », « sauce aux cerises » et « set de pneus d'hiver ». Ce procédé instaure une suite logique qui est brisée par le dernier élément. Le rire est ainsi provoqué par la surprise. Les jeux de mots sont également bien prisés par les Pic-Bois, comme le montre cet exemple : « \[...\] ça marche tu à ton goût? Ben là si je comprends bien, c'est rendu toi qui marche s! ». Le rire est provoqué par une syllepse (l'utilisation d'un même mot dans deux sens différents). Finalement, le calembour : ce type de jeu de mots basé sur la similitude entre deux mots différents : « On jase, on jase...On jazz! ». La scène est également ponctuée d'expressions, de mimiques et de bruitages qui servent, d'une part, à ancrer les personnages dans leur univers singulier et, d'autre part, à provoquer le rire chez le public lorsqu'il reconnait ces éléments tout au long du numéro, à la manière d'un *running gag*. De plus, il y a beaucoup de moments durant lesquels les deux personnages s'expriment en même temps. Il devient alors très difficile pour le public de les comprendre. Encore une fois, cela contribue à affirmer la complicité du duo tout en provoquant le rire devant l'incompréhension ainsi générée.

## Conclusion
Comme on le voit dans ce numéro, les Pic-Bois occupent certainement une place en marge de l'humour convenu. La majorité de leurs numéros donne en spectacle des personnages qui, bien qu'issus de leur imagination débridée, trouvent leur inspiration dans la vraie vie. Les deux humoristes sont en ce sens de très bons observateurs de la société. Tout comme l'oiseau dont ils portent le nom, les Pic-Bois font du bruit en mettant de l'avant les aspects illogiques de la société qu'ils peuvent exagérer à loisir. Leur humour, souvent qualifié de « niaiseux », attire à la fois par son côté sympathique et totalement affranchi des contraintes habituelles. Les Pic-Bois prônent avant tout la liberté de rire, ce qui donne à leurs numéros un côté rafraîchissant et original.

## Pour en savoir plus
Tardif, D. (2019, 28 juin). "Les Pic-Bois et l'art noble de l'humour niaiseux". *Le Devoir*. https://www.ledevoir.com/culture/theatre/557676/les-pic-bois-et-l-art-noble-de-l-humour-niaiseux

[^1]: Dominic Tardif. (2019, 28 juin). *Les Pic-Bois et l'art noble de l'humour niaiseux*. Le Devoir*.* https://www.ledevoir.com/culture/theatre/557676/les-pic-bois-et-l-art-noble-de-l-humour-niaiseux
