---
title: "Charles Beauchesne"
date: 2020-05-06
author: "Mélina Soucy"
youtube_id: xU7tlD-lKLQ
description: "Un artiste à l'humour cynique."
draft: true
portrait: /img/artistes/charles-beauchesne.svg
---
## Biographie et accomplissements
<span class="lettrine">C</span>harles Beauchesne obtient son diplôme de l'École nationale de l'humour en 2010. Depuis cette date, l'humoriste reconnu comme étant un « véritable maître de l'étrange[^1] » a diversifié ses activités. En plus de pratiquer le stand-up régulièrement, il est chroniqueur à l'émission radiophonique *La soirée est encore jeune* et a écrit des chroniques bi-mensuelles pour *Urbania* de 2015 à 2019. Son partenariat avec *Urbania* se prolonge avec la production d'un populaire balado, *Les pires moments de l'histoire avec Charles Beauchesne*, où il commente des faits historiques sinistres avec humour. Ce balado, né en fin 2019, continue de faire rire et d'informer ses fans et ce, même en pleine pandémie.

En dix ans de carrière, l'artiste dont l'humour cynique est très inspiré des dessins animés[^2] a produit six one-man shows: *Bienvenue dans mon cauchemar* (2014), *Charles Beauchesne parle de la peste noire pendant 60 minutes* (2015), *60 minutes de fucking Charles Beauchesne* (2017), *Tout ceci n'est qu'une sombre farce* (2017), *Les Corbeaux me regardent d'un air bizarre* (2019) et *Charles Beauchesne parle de Jack l'Éventreur en 60 minutes* (2019).

Habituellement, Charles mémorise ses textes de stand-up. Mais pour ses spectacles-conférences humoristiques *Charles Beauchesne parle de Jack l'Éventreur en 60 minutes* et *Charles Beauchesne parle de la peste noire pendant 60 minutes*, il n'apprend pas ses textes par cœur, bien qu'ils soient écrits intégralement. Ses deux spectacles sont conçus comme des histoires commentées de façon humoristique. C'est ce qui lui permet d'improviser et de ne retenir que les blagues importantes comme points de repère de son texte[^3].

On a pu le voir à la télévision à *En route vers mon premier gala* aux éditions 2011, 2013 et 2014, ainsi qu'au Grand Rire comédie club en 2011, 2012 et 2014. Charles est apparu à l'émission Trait d'humour en 2016 et faisait aussi partie des invités de l'Open Mic de Virginie Fortin à V en 2019[^4]. Il a également fait partie de la première cuvée d'artistes à lancer le festival d'humour indépendant Dr. Mobilo Aquafest en 2016.

Passionné du Moyen Âge, Charles Beauchesne a transposé son personnage Yorick à la scène. Avec son ami l'humoriste Jessy Sheehy, il présente un spectacle thématique intitulé *Yorick et Pierrick présentent le Comédiéval Show* au Zoofest 2019. C'est en puisant dans des références historiques lugubres et dans l'univers des dessins animés qu'il écrit ses textes humoristiques.

## Présentation du numéro
*Médiéval fantastique!* est un numéro présenté lors de la soirée de rodage *La Niche*, soirée qui convient parfaitement à l'humour niché de Charles Beauchesne. L'humoriste nous y invite à partager son amour inconditionnel pour l'univers médiéval fantastique, le genre qu'on associe à la saga du Seigneur des Anneaux par exemple. Il construit ensuite son numéro autour de l'incohérence de la coexistence entre cet univers extrêmement dangereux et l'époque historique difficile à laquelle il est associé. La prémisse de son numéro est donc de faire remarquer au public à quel point il est bizarre d'ajouter des créatures fantastiques féroces tel que des orcs au Moyen-Âge, période historique où le quotidien était périlleux.

{{< bloc >}}
Ce qui est drôle ici, ce n’est pas nécessairement la situation fictive décrite par l’humoriste, puisqu’il s’agit d’une situation normale pour l’époque, mais bien la justesse de son imitation de vieille mégère moyenâgeuse.
{{< /bloc >}}

## Analyse du numéro
Charles Beauchesne pratique un stand-up généralement thématique. Dans ce cas-ci, la thématique est, comme le nom du numéro l'indique, l'incohérence de l'univers médiéval fantastique. Dès son arrivée sur scène, le persona, c'est-à-dire la présence scénique de l'humoriste, est clairement défini. Bien qu'il ne soit pas costumé, Charles arbore toujours des vêtements sombres, une attitude pessimiste et un regard comique lugubre sur le monde. Il commence souvent son numéro, comme c'est le cas ici, par une remarque mélancolique sur la vie: « quelle vie de chiotte ». Il met ainsi la table à son public, qui s'attendra dès lors à rire de l'étrangeté et de la dureté du monde.

Pour souligner la misère et la solitude de son persona, il introduit le thème de son numéro avec une courte blague : il a recommencé à jouer à Donjon et Dragons (un jeu de rôles de cape et d'épée), car il n'a pas besoin de la chaleur du corps d'une femme. Il appuie cette remarque d'un geste exagéré des bras soulignant son incompréhension et attirant ainsi la sympathie du public. Son jeu non-verbal est aussi important que son jeu verbal. Les deux sont ouvertement hyperboliques afin de rendre son sarcasme clair auprès de son auditoire. À partir de ce moment, ce dernier peut adhérer à l'humour de l'artiste, car il l'associe facilement au stéréotype du « geek qui a de la difficulté avec les femmes et se réfugie dans des univers fictifs. »

La force du stand-up de Charles Beauchesne se trouve dans ses personnages dramatiques, c'est-à-dire les personnages dont il peuple ses histoires et qu'il incarne pendant un moment par la suite. Il joue toujours ses personnages de façon très physique, avec son corps et sa voix, mettant ainsi en suspens, pendant un bref instant, son rapport direct avec le public. Par exemple, lorsqu'il explique, dans la première minute de son numéro, que la chose la plus dangereuse au Moyen Âge, c'est l'époque en soi, il imite une vieille dame qui crie « pendez-le! », à un autre personnage fictif enfermé dans un pilori (poteau de bois servant à exposer temporairement des criminels). Ce qui est drôle ici, ce n'est pas nécessairement la situation fictive décrite par l'humoriste, puisqu'il s'agit d'une situation normale pour l'époque, mais bien la justesse de son imitation de vieille mégère moyenâgeuse. Son visage déformé par la haine factice de ce personnage provoque assurément l'hilarité, puisqu'il surprend après l'énumération de situations dangereuses qui pouvaient se produire au quotidien au Moyen Âge.

Ces personnages dramatiques exagérés, mais toujours tirés d'œuvres fantastiques, demeurent crédibles, car n'importe quel spectateur ayant vu un film ou un dessin animé du genre les reconnaîtra. Ce qui est drôle, c'est l'amalgame de la description faite par Charles et son interprétation. Dans la deuxième partie du numéro, il fait remarquer au public que, en plus de toutes les maladies et de tous les dangers qui guettent la population moyenâgeuse, dans l'univers de Donjon et Dragons, on peut croiser des créatures sans scrupules qui ne désirent que notre mort dès que l'on sort du village. Il prend d'abord les orcs en exemple. Pour s'assurer que tout le monde comprend bien ce qu'est un orc, il les décrit comme des personnages aux visages de raisins secs dans le Seigneur des Anneaux. Cette comparaison crée un premier punch au segment sur ces créatures. La blague suivante sur le sujet repose sur la capacité de Charles à imiter parfaitement les cris des orcs. C'est encore une fois l'incarnation de son personnage dramatique qui provoque le rire. Même un spectateur ignorant ce qu'est un orc pourrait rire, surpris par le bruit étrange produit par l'humoriste.

Pour pousser la blague plus loin, Charles incarne ce à quoi ressemblerait un orc sympathique. L'incompatibilité logique entre l'orc et le sentiment de sympathie provoque un premier rire. Puis, le jeu de l'humoriste vient confirmer le caractère comique de la situation incongrue.

## Conclusion
En 2017, Charles Beauchesne écrivait dans une chronique pour *Urbania* que « ça va bientôt faire sept ans qu'\[il est\] ce qu'on appelle dans le jargon un humoriste "de la relève"[^5] ». Aujourd'hui, il est encore considéré comme faisant partie de la relève et ce, après dix ans de métier. Son humour, à mi-chemin entre le dessin animé et l'horreur, est pourtant arrivé à maturité et réussit à rejoindre de plus en plus de gens grâce à son podcast sur les pires moments de l'histoire. Il fait également partie de la programmation du premier festival d'humour numérique le Hahaha Fstvl, où il présente une nouvelle fois *Charles Beauchesne parle de la peste noire pendant 60 minutes*.

## Pour plus d'infos

Page Facebook: [https://www.facebook.com/CharlesBeauchesneOfficiel](https://www.facebook.com/CharlesBeauchesneOfficiel)

Les pires moments de l'histoire: [https://podcasts.apple.com/ca/podcast/les-pires-moments-de-lhistoire/id1455639655](https://podcasts.apple.com/ca/podcast/les-pires-moments-de-lhistoire/id1455639655)

[^1]: Union des Artistes. Charles Beauchesne. 2020. \[https://uda.ca/utilisateurs/132161\]

[^2]: NousTV Rimouski. Entrevue avec Charles Beauchesne. 17 mai 2018. \[https://www.facebook.com/268586003243317/videos/1410320195736553/?v=1410320195736553\]

[^3]: Levac, Thomas. Le Podcast de Thomas Levac, Épisode 31: Charles Beauchênes, 7 avril 2020. \[https://www.youtube.com/watch?v=-oj-BmU51-g\]

[^4]: Juste pour rire. Artiste: Charles Beauchesne, 2020. \[https://www.justepourrire.com/evenements-corporatifs/nos-artistes/charles-beauchesne\]

[^5]:  Charles Beauchesne. Artiste pas connu: un moment donné il faut assumer.Urbania, 13 juillet 2017 \[https://urbania.ca/article/artiste-connu-moment-donne-faut-assumer/\]
