---
title: "Yvon Deschamps"
date: 2020-05-06
author: "Étienne Fay-Milette"
youtube_id: qJdaqXBUMw4
draft: true
description: "Parler de Yvon Deschamps, c'est parler d'un monument de l'humour."
portrait: /img/artistes/yvon-deschamps.svg
---
## Le bonheur
Parler de Yvon Deschamps, c'est parler d'un monument de l'humour. Le plus grand ironiste que la scène ait connu. Avec Clémence Desrochers et Dominique Michel, il donne ses lettres de noblesse au métier d'humoriste au Québec, ce « sont de véritables pionniers. »[^1] Ses monologues traversent le temps et, contrairement à d'autres, il n'utilise généralement pas de sujets tirés de l'actualité. Il mise sur la vie en soi.

{{< bloc >}}
Grâce à l’autodérision, il permet à son public de se moquer de lui sans gêne.
{{< /bloc >}}

Dans ses changements, son mouvement, son évolution. Celle du quotidien, mais un quotidien minimaliste que l'on pourrait transposer d'époque en époque. Il mise sur la simplicité et sa portée singulière s'étale à toutes les tranches d'âge. Si j'ai à choisir un monologue parmi son œuvre, j'avoue miser sur la simplicité à mon tour. Je me lance dans l'analyse d'un de ses plus célèbres monologues : *Le bonheur*.

## La place de la chanson
Pour Deschamps, l'humoriste ne doit pas d'abord être drôle, au contraire, et l'humour est une discipline artistique à part entière. Il y a tout ce que l'on veut bien y mettre : des personnages, de la chanson, du mime. Puisqu'il est d'abord acteur et chanteur, Deschamps sait utiliser son corps et sa voix pour dynamiser son message avec aisance. La chanson ne quitte jamais le personnage qu'il incarne sur scène. Et puisque le *setup* est rarement drôle, car il n'existe que pour donner une mise en contexte au public, alors pourquoi ne pas le présenter en chantant ? C'est une méthode surprenante pour commencer un numéro, mais il faut ajouter que, à son époque, le métier n'est pas aussi formaté qu'aujourd'hui. Tous les coups sont permis.

## Chialer pour chialer
Le constat de départ : tout le monde chiale. Même lorsqu'ils s'arrêtent une minute, les gens recommencent juste après. Ce qui est intéressant, c'est qu'il va lui-même chialer à propos des gens qui chialent. C'est un cercle vicieux qui plonge son discours dans l'ironie. Sauf que, en même temps, il souligne que les gens ne s'aperçoivent pas de ce qu'ils ont et des belles affaires gratis auxquelles ils ont accès. Cela pique la curiosité. Il module sa voix pour faire sentir une certaine improvisation (hésitations, questionnements, répétitions). Le rapport de force semble s'inverser, car il perd la solidité de sa défense. Sa crédibilité est volontairement fragilisée. Le choix de son vocabulaire est efficace, se limitant au registre familier afin de donner des références connues de tous et de rester dans le discours populaire. Il ne se prend pour un être supérieur qui détient la grande vérité sur le bonheur, au contraire, il est dans le même bateau que les autres. Puis, grâce à l'autodérision, il permet à son public de se moquer de lui sans gêne.

Ce qui m'intéresse plus précisément, c'est sa façon de désacraliser la messe en y transposant les rites normaux d'un hôte moyen. Il y juge la nourriture, l'égoïsme (à propos de l'alcool), mais trouve quand même la messe plus avantageuse que les pilules chimiques pour faire la sieste. C'est en quelque sorte une opinion que les gens n'os(ai)ent pas dire à voix haute, mais qu'ils vont tout de même approuver. L'humoriste dialogue ainsi avec la foule qui répond par l'entremise du rire. Ça lui permet d'en ajouter pour tourner la religion en ridicule (de manière inoffensive) en recourant à la raillerie. Grâce à un fin mélange de blagues et d'émotions, Deschamps surprend et son public ne peut prédire la conclusion de son numéro.

## Monsieur Bonheur
Pour mettre à l'aise son auditoire, l'humoriste confirme qu'il est un épais et qu'il ne connaît rien. Il brise le rythme pour mieux s'aligner vers la chute. Afin qu'un monologue soit un succès, l'histoire présente un début, un milieu et une fin, mais surtout, un fil conducteur que le spectateur doit saisir avec clarté. La chute, de son côté, doit se lier à la prémisse à travers un crescendo capable de créer la surprise. Même si cette recette gagnante semble simple, elle ne l'est pas pour tout le monde. Deschamps, lui, opte pour une personnification du bonheur afin de transformer son discours moralisateur en un discours personnel. Il donne son point de vue sur Monsieur Bonheur avec authenticité, s'appuyant sur des exemples où il se manifeste et ne se manifeste pas, puis témoignant de sa relation (ou non-relation) avec lui.

En présentant l'exemple de la famille « à l'aise », il joue sur les mots et utilise le sarcasme pour vanter le fait que le père a « son propre lit de mort », avec admiration. Comme, quoi c'est toujours les mêmes qui ont tout. Sa prémisse, où il mentionne que « chacun pourrait en profiter » et que « les plus belles choses ne coûtent rien », semble quelque peu bousculée. L'humoriste précise les critères, les exigences et l'attitude nécessaires pour héberger Monsieur. Il ajoute certaines nuances en considérant que les enfants de la famille « à l'aise » n'auront plus le bonheur puisqu'ils ne peuvent pas attendre le décès du père avant de se battre pour l'héritage. Le bonheur n'est plus vraiment « à la portée de tout le monde ». Même si Deschamps donne l'impression d'un sujet très simple au départ, il va se complexifier petit à petit jusqu'à ce qu'il avoue qu'il ne l'a jamais connu. Il passe du sermon à la confession.

## Faire partie de la famille
La beauté de ce monologue, c'est qu'il passe d'une critique généralisée pour finalement incarner le sujet de sa critique. Sa mise en garde ne sert qu'aux autres puisque, dans son cas, il est déjà trop tard. Sa femme « tombe malade d'urgence » (excellent jeu de mot) et le bonheur déteste la maladie. Lui-même va bientôt y passer. En croyant voir le bonheur au loin, il devient celui qu'il qualifie de « jaloux », qui s'exclame et repousse le bonheur en le voyant chez les autres. Il donne l'exemple parfait de ce qu'il ne faut pas faire pour l'attirer. Modifier ainsi la perspective permet de passer un message et de le soutenir avec un exemple concret. Il permet une réflexion plus large sur le sujet grâce aux mécanismes de l'humour.

{{< bloc >}}
Grâce à un fin mélange de blagues et d’émotions, Deschamps surprend et son public ne peut prédire la conclusion de son numéro.
{{< /bloc >}}

Au départ, le personnage reprend le discours qu'il a entendu sur le bonheur, puis en rapportant son histoire personnelle, il se montre de plus en plus sincère et vulnérable. Sa transparence donne de la véracité à ce qu'il vit et le spectateur est plus facilement touché par son désespoir. Si ce monologue perdure, c'est parce qu'il traite d'un sujet intemporel, mais avec un angle personnel et innovateur. La signature de Deschamps, c'est de prendre des thèmes complexes et de les simplifier au maximum afin de rejoindre l'ensemble de la société. Sa mise en scène fait croire à de l'improvisation. Sa maîtrise de la langue fait croire qu'il ne fait pas exprès pour faire rire. Et sa complicité avec le public nous donne l'impression qu'il fait partie de la famille. C'est ce grand naturel (travaillé) sur scène qui distingue Deschamps des autres. Il reste lui-même et fait de l'humour sans sembler forcer. Comme quoi il est en parfaite maîtrise de son art. L'humoriste donne déjà les bases de sa formule gagnante, grâce à l'héritage et la richesse de ses monologues, à ceux qui définissent à leur tour le métier d'humoriste et reprennent le flambeau.

[^1]: D'AMOUR, Valérie & LEBEAU, Mireille, « Des géants de l'humour », *À rayons ouverts*, Bibliothèque et archives nationales du Québec, n°92, Printemps été 2013.
