---
title: "Mélanie Ghanimé"
date: 2020-05-06
author: "Antoine Fauchié"
description: Chaque numéro est une explosion de gags mélangeant autodérision et interrogations sociales.
draft: true
portrait: /img/artistes/melanie-ghanime.svg
---
Numéro disponible sur la plateforme Noovo : [https://noovo.ca/videos/lopen-mic-de/le-stand-up-complet-de-melanie-ghanime](https://noovo.ca/videos/lopen-mic-de/le-stand-up-complet-de-melanie-ghanime)

## Présentation de Mélanie Ghanimé
Mélanie Ghanimé est une humoriste québécoise née à la toute fin des années 1970 et basée à Montréal.
Elle sort de l'École nationale de l'humour en 2009, parmi une promotion composée au total de 5 femmes et de 9 hommes.
La question de l'équité est importante pour Mélanie Ghanimé comme nous le verrons plus tard.
Mélanie Ghanimé a beaucoup de réalisations à son actif : elle a notamment fait la première partie des spectacles de Lise Dion en 2015, depuis ses débuts elle participe à de nombreux galas (dont le Zoofest et Juste pour rire), elle intervient régulièrement à la télévision ou dans des webséries (comme _[Adrienne](https://www.youtube.com/playlist?list=PLexgVWwfAEvyD1EeSkzcfftHFaqW60bfE)_ avec son compagnon et qui a comme sujet sa propre fille), et elle a plusieurs spectacles one-woman-show à son actif dont _Je me prépare_ en 2018 et _BRUT[e]_ depuis 2019 qui est toujours en représentation en 2020.

Parmi les retours de la presse ou de la critique sur ses numéros on peut noter quelques points récurrents :

- la place de son discours, « elle a des choses à dire et sait comment les dire » (Marie-Josée R. Roy, Journal de Montréal / TVA), « efficacité de ses textes » (François Houde, Le Nouvelliste, 12 juin 2018) ;
- son _franc-parler_ : « un mélange entre un pitbull et le doux printemps » (PA Méthot), « elle a du punch" (Laurent Paquin), « entière, intense et drôle » (Catherine François, TV5). Sur scène elle sait être incisive et percutante par ses choix de sujet et sa façon de les aborder (de façon franche).

Comme elle le dit elle-même, elle fait partie des 20 artistes femmes dans un milieu de l'humour majoritairement masculin.
Elle a pris position sur le problème du sexisme dans le milieu de l'humour lors du gala Des Olivier en 2019 avec Silvi Tourigny ([voir la vidéo](https://www.youtube.com/watch?v=6snumGO68yM)).
Pendant près de 5 minutes, elles exposent en duo le problème de la reconnaissance des femmes dans le milieu de l'humour, la prédominance de l'humour masculin et les préjugés sur les femmes humoristes (qui ne manquent malheureusement pas...).
Elles soulignent le fait qu'il y a finalement peu de femmes dans ce domaine, ou plutôt peu de femmes reconnues, malgré les effets d'annonce du type "les choses ont changé".
Le public n'est pas non plus épargné, marquant une situation qui dépasse largement le monde du comique.
On peut noter que ce numéro a été réalisé devant un public composé d'artistes de l'humour, et qu'il fallait donc un certain courage pour exposer une réalité devant ses pairs.

## Présentation du numéro
Le numéro de Mélanie Ghanimé n'a pas d'intitulé particulier, il est présenté lors de l'émission Open Mic en mars 2019, une soirée animée par l'humoriste Katherine Levac.
Il s'agit d'une histoire personnelle lors d'une journée "à la plage", où elle décrit comment elle se retrouve dans une situation délicate avec un pédalo.

## Analyse du numéro

### Remarques générales
L'humoriste joue beaucoup avec son corps : mouvements mimés, changement de tonalité de sa voix, utilisation de l'espace de la scène, regards vers le public.
Si Mélanie Ghanimé semble raconter une histoire devant des amis, son texte est en fait très bien écrit, chaque mot est pesé pour conserver un rythme haletant et un vocabulaire accessible à tous.

### Thématique
La thématique principale est la peur, ou comment réussir à se sortir d'une situation qui peut nous angoisser – ici la peur irrationnelle d'une bête dans l'eau, puis la peur du ridicule.
Mais c'est loin d'être le seul sujet du numéro, car il est aussi question de sexisme, en creux, pendant ces 7 minutes.

### Structure
L'introduction du numéro prend la forme d'une suite de blagues qui permettent à Mélanie Ghanimé de poser tous les éléments nécessaires : le contexte général (l'été), la pression sociale exercée sur les femmes et leur corps, le manque d'argent des personnages et la misanthropie de l'humoriste (elle précise qu'elle aime les gens, mais lorsqu'ils sont assis dans une salle).

Je ne vais pas tenter de résumer le numéro que vous venez de voir, mais voici quelques points qu'il me semble intéressant de souligner.
Tout d'abord le fait que Mélanie Ghanimé donne beaucoup de détails qui nous permettent de nous plonger (c'est le cas de le dire...) dans son récit.
En moins de 2 minutes, nous avons tous les ingrédients utiles pour nous figurer précisément l'histoire, que ce soit en terme :

- de temps : "y'a juste une joie de l'hiver, c'est de savoir que l'été va revenir" ;
- de personnages : elle et son ami ;
- d'espace : "on est allé à la _playa municipale du Lac Memphrémagog_ [imitation de l'accent espagnol]" ;
- d'espace-temps : "ça faisait une demi-heure que j'étais parti [en pédalo] j'étais toujours en pleine conversation avec le gars qui me l'avait loué".

Mélanie Ghanimé suit un fil narratif et ne le quitte que très rarement.

Il est intéressant d'observer le rythme du numéro, il y a une accélération sur les deux dernières minutes qui laisse peu de répit au public.
Mélanie Ghanimé parle de plus en plus vite, tout en conservant de fréquents ralentissements pour laisser le public respirer (et rire).
Loin d'être une saturation, on pourrait plutôt qualifier le final d'une série d'explosions permises par un placement minutieux de détails depuis le début.
La chute finale n'est pas non plus le moment le plus drôle du numéro, mais un point d'arrêt à une série resserrée de déflagrations comiques.

### Procédés
Mélanie Ghanimé utilise plusieurs procédés comiques, voici une liste qui est loin d'être exhaustive :

- rapport au réel : elle raconte des histoires qui semblent réelles, et qui pourraient être les nôtres. Elle prend régulièrement le public à partie pour s'accorder avec lui sur des considérations communes, et ainsi donner plus d'impact à ses propos et faire rire plus fort ;
- énumération : elle utilise régulièrement ce procédé qui consiste à lister rapidement un certain nombre d'éléments pour aboutir à une absurdité ;
- autodérision : elle se met en scène (elle ou le persona qui lui ressemble) tout en se moquant d'elle-même, et notamment de son corps (qu'elle ne dénigre pas pour autant), de sa condition physique, de ses finances ou de ses peurs ;
- silences : Mélanie Ghanimé rythme son numéro et délimite certaines blagues en utilisant fréquemment les silences. Elle laisse le temps au public pour rire, ou lui indique quand une blague est terminée ;
- mime et imitations : elle utilise son corps pour signifier des situations, accompagner le texte par des modulations de voix et par des gestes. Mélanie Ghanimé implique tout son corps dans ce numéro.

## Conclusion
Ce n'est pas parce que Mélanie Ghanimé a fait la première partie d'un spectacle de Lise Dion qu'il faudrait se poser la question d'une éventuelle filiation.
Leurs points communs sont probablement l'autodérision et le fait de parler de leur corps (un peu), mais c'est ce qui occupe tout de même la grande majorité des textes des humoristes, femmes et hommes.
Mélanie Ghanimé aborde des questions contemporaines et notamment la place des femmes dans la société, ou plutôt la place disproportionnée des hommes, et d'autant plus dans le milieu humoristique (Joubert, 2010).
Elle ne s'interdit pas de centrer certains de ses numéros sur des sujets qui nous concernent toutes et tous : l'achat d'une auto, la légalisation du pot, l'éducation sexuelle, etc.
Chaque numéro est l'occasion de dénoncer des situations inégalitaires, avec humour et fermeté.

Lorsque l'on regarde beaucoup de numéro de stand-up de femmes, on peut se rendre compte que, souvent, les présentations des humoristes par les MC sont axées sur des caractéristiques particulièrement liées au genre : physique, maternité, partenaire de couple masculin, etc.
Ce n'est pas le cas de Mélanie Ghanimé : même si elle fait une websérie sur sa fille, elle n'est pas présentée comme mère ; même si elle parle de son corps dans ses numéros, elle n'est pas présentée selon son physique.
C'est aussi un point qu'évoque Mélanie Ghanimé avec Silvi Tourigny lors de leur numéro au Gala Des Olivier : les femmes sont souvent réduites à leur genre ou leur "sex appeal".
La question serait de savoir si c'est Mélanie Ghanimé qui demande au MC de la présenter sans sexisme ou si c'est une situation de fait.

Mélanie Ghanimé est une figure à la fois singulière et emblématique dans le milieu québécois de l'humour.
Singulière car elle aborde frontalement certains sujets, et emblématique car elle représente bien une partie de la génération "de la relève" qui veut faire rire sans compromis de certaines situations.
Le regard de la société sur les femmes est l'un de ses sujets, sans pour autant se revendiquer de l'humour politique, et tout en usant de certains stéréotypes pour les détourner ou les décaler.
Mélanie Ghanimé n'abdique ou ne fléchit pas devant ce qui peut sembler être la normalité, elle remet en cause des préjugés, des inégalités, que ce soit de son côté ou de celui du public.
Dans ses numéros ou dans ses interventions, Mélanie Ghanimé utilise le quotidien et en accepte certaines contraintes, sans oublier de remettre en cause des modèles.

## Pour en savoir plus

### D'autres créations de Mélanie Ghanimé

- Son numéro avec Silvie Tourigny lors du gala Les Oliviers en 2019 : [https://www.youtube.com/watch?v=6snumGO68yM](https://www.youtube.com/watch?v=6snumGO68yM)
- Son numéro "La légalisation" lors de l'émission L'heure est grave : [https://zonevideo.telequebec.tv/media/41264/melanie-ghanime-et-la-legalisation/l-heure-est-grave](https://zonevideo.telequebec.tv/media/41264/melanie-ghanime-et-la-legalisation/l-heure-est-grave)
- Sa websérie "Adrienne" avec son compagnon et sa fille : [https://www.youtube.com/playlist?list=PLexgVWwfAEvyD1EeSkzcfftHFaqW60bfE](https://www.youtube.com/playlist?list=PLexgVWwfAEvyD1EeSkzcfftHFaqW60bfE)

### Bibliographie succincte

Joubert, Lucie. 2010. « Rire : le propre de l’homme, le sale de la femme ». Dans _Je pense donc je ris. Humour et Philosophie_, 85‑101. Laval: Presses de l’Université de Laval.
