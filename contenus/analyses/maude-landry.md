---
title: "Maude Landry"
date: 2020-05-06
author: "Émilie Coulombe"
video_alt: https://ici.radio-canada.ca/premiere/emissions/la-soiree-est-encore-jeune/segments/entrevue/60730/numero-humour-entrevue-humoriste-maude-landry
description: "Un examen faussement ingénu, critique, incisif et drôle de la société."
draft: true
portrait: /img/artistes/maude-landry.svg
---
## Présentation de Maude Landry
<span class="lettrine">J</span>eune *stand-up comic* de la relève, Maude Landry occupe une place de plus en plus enviable dans l'industrie de l'humour.

En 2011, elle obtient un diplôme d'études collégiales en Arts et lettres qui l'amène à poursuivre des études universitaires en traduction (français/anglais). Landry décroche finalement un certificat dans ce domaine en 2014. Parallèlement à son parcours académique, elle fréquente de nombreuses soirées micro ouvert et suit, en 2013, les cours de perfectionnement du soir à l'École nationale de l'humour (ÉNH), tant sur l'écriture humoristique, la présentation de numéros que la scénarisation pour la télévision. L'humoriste de 28 ans continue néanmoins à être présentée comme une autodidacte dans son milieu, puisqu'elle n'a jamais été inscrite au cursus professionnel régulier de l'ÉNH pour des motifs principalement financiers. Sa formation à la dure, acquise dans les bars du Québec, ne l'a toutefois pas empêchée de faire son chemin, puis de rayonner dans l'industrie.

Sur scène, on a entre autres pu la voir à l'œuvre au Bordel Comédie Club de Montréal, où elle se produit en spectacle depuis 2016, au ComediHa! Club de Québec et, en tant qu'animatrice, aux Mardis du rire du bar le Bungalow à Longueuil. Attirant un public intergénérationnel et bigarré, elle a aussi fait les premières parties des spectacles de Louis T. et d'Adib Alkhalidey en 2017. Son premier spectacle solo, *Subtile*, a été présenté au Théâtre Rialto en avril 2018 dans le cadre du festival **Dr. Mobilo Aquafest**. Actuellement en tournée de rodage à travers le Québec, Landry peaufine l'écriture de son prochain *one-woman-show,* avec lequel elle espère intégrer, pour la première fois, le circuit des scènes institutionnalisées.

À la télévision, elle a notamment collaboré aux émissions *Info, sexe et mensonges*, *Les pêcheurs*, *Alt*, *Patrick Groulx et ses p'tits pas fins* et *\#5règles*. Elle présente aussi régulièrement des chroniques radiophoniques à l'émission *La soirée est (encore) jeune*. Récemment, on a même pu la voir au cinéma dans le film *Les barbares de la Malbaie*. Bref, Maude Landry, dont le rêve est de prendre part à un *open mic* new-yorkais, investit plusieurs plateformes de son humour, bien que le stand-up demeure son art de prédilection. C'est en effet micro en main, dans un espace à peine jonché d'un tabouret et d'une guitare -- car il lui arrive de chanter des résumés de films sur fond de thèmes musicaux populaires --, qu'elle s'adresse le plus souvent à son public.

{{< bloc >}}
L’humoriste interroge les on-dit répétés autour de nous et transcende de la sorte l’observation simpliste du quotidien.
{{< /bloc >}}

## Présentation du numéro
En 2018, Maude Landry devient la deuxième femme -- après Katherine Levac -- à être sacrée Découverte de l'année dans l'histoire des Oliviers. Lors de ce même gala, elle remporte aussi le prix de la meilleure capsule radio humoristique pour « Les choses pas logiques », qui a été enregistrée lors d'une émission de *La soirée est (encore) jeune* présentée devant public le 24 février 2018 au Théâtre Corona. Ce numéro, dans lequel elle réfléchit aux éléments insolites du quotidien, reflète bien ses thèmes chers : les combats de la vie de tous les jours, les croyances humaines ou encore les animaux.

## Analyse du numéro

### Thématique
Landry pratique un humour rassembleur loin de déclencher les hostilités. Elle expose un regard naïf sur le monde qui l'entoure et donne souvent l'impression de ne pas saisir ce qu'il y a d'absurde dans ses raisonnements lacunaires, pourtant présentés sous le couvert de la cohérence.

### Structure
C'est d'abord l'accumulation d'images insolites qui frappe dans « Les choses pas logiques ». Si les s*tand-ups* québécois sont reconnus pour enchaîner les effets comiques à vitesse grand V, la plupart d'entre eux les regroupent néanmoins dans un récit englobant. Or Landry compose plutôt des séries de gags qui servent moins l'évolution d'une histoire que le déploiement de différentes unités thématiques. De ce fait, lors d'un premier contact, ses numéros peuvent donner l'impression de relever de l'association libre d'idées. En y regardant de plus près, on remarque toutefois que leur construction, bien qu'en apparence décousue, est mûrement réfléchie. Dans la capsule à l'étude, c'est le syntagme « Il n'est pas logique » qui, sans être explicitement répété, orchestre les transitions d'un sujet à l'autre :

> **(Il n'est pas logique qu'...)**
>
> ... acheter deux billets pour Messmer passe pour une bonne idée de cadeau, alors qu'acheter un seul billet en représente une mauvaise (1 min. 12).
>
> **(Il n'est pas logique que...)**
>
> ... les nouvelles technologies, censées nous aider, nous nuisent parfois (1 min. 36).
>
> **(Il n'est pas logique que...)**
>
> ... les blonds aux yeux bleus soient considérés supérieurs (2 min. 30).
>
> **(Il n'est pas logique de...)**
>
> ... soutenir que les chats n'aiment pas l'eau (3 min. 03).
>
> **(Il n'est pas logique de...)**
>
> ... dire que les hommes en uniforme sont séduisants (3 min. 37).
>
> **(Il n'est pas logique que...)**
>
> ... certains Montréalais se protègent à l'aide de trop basses clôtures (4 min. 16).
>
> **(Il n'est pas logique que...)**
>
> ... les propriétaires recourent aux doublons dans leur nom de compagnie (Pizza Pizza/Manteaux Manteaux) (5 min.).

La phrase « Il n'est pas logique » constitue, autrement dit, le cœur de l'organisation de la composition, qui progresse par effets de boucle.

### Procédés
La réussite du numéro tient également au croisement de plusieurs mécanismes comiques, dont l'incompatibilité logique et les jeux de mots.

Landry met souvent en place un cadre en apparence cohérent à l'intérieur duquel un élément déplacé surgit; c'est en prenant connaissance de cette incongruité que le spectateur rit. Dès le début, par exemple, l'humoriste s'adresse au public du Théâtre Corona en lançant un « Salut Centre Bell » qui déstabilise. De même, elle nous conduit sur le terrain connu de la critique des technologies pour mieux en rompre la logique en rapprochant son « autocorrect » d'un « petit autochtone », ce qui donne l'impression qu'un réel individu la suit partout (1 min. 50). On peut aussi voir dans cette personnification une stratégie d'infiltration, le discours sur les autochtones se glissant dans le contexte étranger de la discussion technologique pour faire rire. À d'autres moments, l'humoriste substitue carrément une logique usuelle (par exemple, l'interdit et le danger excitent les humains) par son contraire : s'appuyant sur l'attrait exercé par les pompiers qui retirent leur uniforme pour poser dans les calendriers caritatifs, Landry conclut que c'est plutôt la sécurité qui excite les gens (3 min. 45). Ici, c'est le renversement de l'idée reçue qui provoque le rire.

L'humoriste accorde également une importance significative aux jeux de mots dans son écriture, attention qu'il faut sûrement comprendre comme une influence de sa formation en traduction et de sa fascination pour les langues. Landry aime tout particulièrement exploiter la différence sémantique d'homophones (termes dont la prononciation est semblable, voire identique). Il n'y a qu'à penser aux calembours « bons **aryens**/bons **à rien** » (2 min. 35) ou encore « débiles m**an**teaux/débiles m**en**taux » (5 min. 41) de la capsule à l'étude. Toute la séquence sur les doublons « Pizza Pizza » et « Manteaux Manteaux » met aussi au jour la sensibilité linguistique de l'humoriste, qui tâche de révéler l'aspect cocasse des mots tapissant notre quotidien.

{{< bloc >}}
Maude Landry travaille à partir d’une série de lieux communs (« les hommes en uniforme sont sexy », « la technologie nous fait gagner du temps », etc.)
{{< /bloc >}}

Enfin, bien qu'on ne puisse pas l'observer, mais seulement le pressentir dans la version radiophonique du numéro, il importe d'insister sur le jeu de Landry, auquel la réussite de l'humour tient considérablement. C'est que le comique, chez elle, naît souvent du décalage entre, d'un côté, les propositions qu'elle donne à entendre et, de l'autre, sa maladresse et ses réactions pince-sans-rire. L'humoriste adopte généralement un faciès imperturbable, une allure gauche ainsi qu'une diction empâtée, relâchée, dont le rythme et le ton ne correspondent pas à la situation racontée, ce qui fait émerger le comique.

## Conclusion
Bref, Landry se tient en fine équilibriste entre l'observation anecdotique, qui fait une place privilégiée aux informations personnelles d'un « je », et l'examen critique de la société. Ses commentaires plus incisifs doivent, cela dit, être décelés sous des remarques en apparence ingénues. À titre d'exemple, dans « Les choses pas logiques », elle travaille à partir d'une série de lieux communs (« les hommes en uniforme sont sexy », « la technologie nous fait gagner du temps », etc.) qu'elle déconstruit à coups de contre-exemples loufoques. Reste qu'en affaiblissant la portée de ces affirmations galvaudées, l'humoriste interroge les on-dit répétés autour de nous et transcende de la sorte l'observation simpliste du quotidien.

## Pour en savoir plus

{{< references >}}
LESSARD, Anita (2019), « La philo-humoriste Maude Landry », *EstriePlus : le journal Internet* \[en ligne\], http://estrieplus.com/contenu-maude/_landry/_humoriste/_releve-1376-47289.html. Page consultée le 7 avril 2020.

« Maude Landry », *Phaneuf* \[en ligne\], https://phaneuf.ca/artistes/maude-landry. Page consultée le 2 mars 2020.

PERRIN, Catherine (2018), « Entrevue avec Maude Landry », *Médium large* \[en ligne\], https://ici.radiocanada.ca/premiere/emissions/medium-large/segments/entrevue/98759. Page consultée le 20 février 2020.

TARDIF, Dominic (2018a), « La soirée des bisous », *Le Devoir* \[en ligne\], https://www.ledevoir.com/culture/543290/le-20e-gala-les-olivier-la-soiree-des bisous. Page consultée le 7 avril 2020.

TARDIF, Dominic (2018b), « Dans l'incubateur des soirées d'humour », *Le Devoir* \[en ligne\], https://www.ledevoir.com/culture/522849/dans-l-incubateur-des-soirees-d-humour. Page consultée le 27 février 2020.
{{< /references >}}
