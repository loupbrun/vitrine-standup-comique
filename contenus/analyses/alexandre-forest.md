---
title: "Alexandre Forest"
date: 2020-05-06
author: "Antoine Fauchié"
youtube_id: YASOdEtNwFA
description: Alexandre Forest est représentatif d'une génération d'humoristes qui cassent les codes établis.
draft: true
portrait: /img/artistes/alexandre-forest.svg
---
## Présentation d'Alexandre Forest
Alexandre Forest est un humoriste québécois de 27 ans.
Sorti de l'École nationale de l'humour en 2016, c'est un artiste de stand-up.
Il initie ou participe également à des émissions, des webséries ou des podcasts.
Alexandre Forest se distingue par les sujets qu'il aborde, volontairement politiques ou sociaux, il est même qualifié d'"engagé".
Les propos de ce jeune humoriste ont comme objectif de faire réfléchir le public, loin de l'humour classique il questionne la place et le rôle de l'homme, défend une position non sexiste, et dénonce la haine.

À l'École nationale de l'humour, Alexandre Forest faisait partie d'une promotion hétéroclite, aux côtés notamment de Simon Boisvert ou de Colin Boudrias.
Depuis 2016, il cumule quelques succès non négligeables, il est notamment lauréat du Concours de la relève 2018 du Festival d’humour en Abitibi-Témiscamingue, et toujours en 2018 il a le prix Coup de coeur de l'équipe du festival Zoofest et il est nommé révélation du festival ComédiHa!
Il participe à plusieurs galas, il collabore à des émissions comme L’heure est grave (saison 2018-2019), il est régulièrement invité à des émissions, et plus récemment il a lancé un podcast avec Hugo Bastien, "En chair et en ondes".
Son spectacle "Les 4 saisons de Forest" devait être programmé en mai 2020.
Son originalité sur scène est probablement pour beaucoup dans ces premières reconnaissances.

Alexandre Forest a une approche pour le moins décalée par rapport au milieu de l'humour québécois pris dans son ensemble.
À travers ses textes, il défend une vision du monde qui n'est pas celle qui transparaît dans les numéros humoristes habituels et, disons-le, bien souvent sexistes (Joubert, 2010).
C'est avec une certaine ferveur qu'il s'oppose à une vision binaire du monde, de la société, des gens, des rapports entre hommes et femmes, entre femmes et femmes et entre hommes et hommes.
Prônant la non-violence il est néanmoins virulent sur scène, prenant le public à témoin.
Alexandre Forest n'hésite pas à se travestir dans certains de ses numéros pour appuyer ses propos – c'est le cas dans certaines représentations de son numéro "Les transphobes" étudié plus loin –, attirant l'attention pour pouvoir faire passer un message.
Il aborde des sujets complexes, tout en jouant d'autodérision et en se prenant comme exemple ou cobaye – c'est le cas avec son numéro sur la psychologie, voir référence en fin de texte.

Pour comprendre un peu mieux cet humoriste, nous analysons son numéro "Les transphobes", représentatif de l'approche et de certains des procédés d'Alexandre Forest.

## Présentation du numéro "Les transphobes"
"Les transphobes" est un numéro de 4 minutes sur la transphobie, comme son titre l'indique.
La transphobie définit l'hostilité envers les personnes transgenres, Alexandre Forest s'intéresse plus particulièrement à la manifestation de la transphobie sur internet.
Pendant quelques minutes l'humoriste démonte les supposés arguments des transphobes, calmement, et avec beaucoup d'humour, à partir d'une expérience personnelle : il a publié un message sur un forum et a récolté quelques commentaires.
Ce numéro a été filmé pendant une soirée organisée par La Niche en 2018, une scène de stand-up éphémère qui se définit elle-même comme "une maison pour tous les chiens errants de l'humour" (https://www.facebook.com/laniche.internet/).
Ce numéro est l'un des plus connus d'Alexandre Forest, et surtout l'un des seuls facilement visionnables sur le web.

## 3. Analyse du numéro

### Remarques générales sur la l’espace, la scénographie, etc.
Alexandre Forest n'est pas un humoriste qui joue particulièrement avec son corps, c'est-à-dire qu'il n'est pas dans le mime et qu'il ne traverse pas la scène en long et en large pendant ces numéros.
En revanche il joue avec les codes du genre et le signifie avec des textes travaillés et parfois avec des tenues originales (notamment des bijoux pour certaines représentations de ce numéro).
Dans cette représentation de "Les transphobes", la salle est assez petite, le public constitué d'une cinquantaine de personnes.
Contrairement à d'autres représentations, ici il prend son temps, il laisse le public rire – et rit lui-même aussi –, il semble plus à l'aise avec son texte.

### Thématique
La thématique de la transphobie est habituellement peu abordée dans des numéros humoristiques.
Ce choix est singulier, et à ce titre il rejoint d'autres humoristes parfois qualifiés de politiques comme par exemple Colin Boudrias – lui aussi sorti de l'École nationale de l'humour en 2016, et qui parle de gentrification également à La Niche.
C'est une marque de fabrique d'Alexandre Forest, d'aborder des sujets considérés difficiles comme la transphobie, l'angoisse, l'hypocondrie, l'homosexualité, le suicide, etc.
Si cela peut paraître trop sérieux pour un numéro de stand-up, Alexandre Forest parvient au contraire à dédramatiser par une approche frontale, et en utilisant des processus de démonstration simples et sans compromis.
Il s'adresse aux spectateurs avec patience et pédagogie, sans pour autant les prendre pour des imbéciles.
L'objectif de cet artiste déterminé est, semble-t-il, de faire réfléchir en faisant rire, et inversement.

### Structure
Dans une courte introduction Alexandre Forest explique qu'il a posté un commentaire en faveur des transgenres :

> Je pense que c'est correct de vouloir changer de sexe, ça n'a aucun impact sur ma vie, et tant mieux si ça rend des personnes heureuses.

Ensuite, il présente certains commentaires transphobes.
Ce numéro de stand-up est donc une énumération, avec comme fil rouge la thématique de la transphobie, ou plus généralement la faiblesse des arguments de ceux qui n'aiment pas ce qu'ils ne comprennent pas.
À travers les 4 ou 5 commentaires déconstruits inlassablement par l'humoriste, le numéro se renforce, les propos d'Alexandre Forest prennent de l'ampleur et de la profondeur, il nous convainc.
Quelques éléments récurrents permettent de donner une structure et de rebondir régulièrement jusqu'au final, en respectant un rythme d'un gag toutes les 4 ou 5 phrases.
Les moments _non comiques_ ont comme seuls objectifs de donner plus de force aux blagues.

### Procédés
Alexandre Forest utilise plusieurs procédés, dont voici certains détaillés.

Tout d'abord la vérité : le point de départ du numéro est un fait raconté par l'humoriste et qui, de plus, est vécu et provoqué par lui-même.
Le public est ainsi convié dans une part de réel, qui le concerne aussi puisqu'il s'agit d'un fait de société.
Ce procédé est très utile pour retenir l'attention, et pour ensuite tisser un récit ou des blagues qui auront ce socle en commun.

Ensuite l'insistance par la répétition, Alexandre Forest utilise deux éléments récurrents pour structurer son numéro.
Le commentateur transphobe très agressif – il dit notamment "tu as une maladie mentale qui peut se soigner uniquement par une balle dans la tête" – et le volley-ball, sont deux moyens de rebondir pendant le numéro, jusqu'au final.

Pour une des blagues l'absurdité est utilisée dans une comparaison : "Comment fonctionne le Dow Jones ? Comme les bateaux à vapeur !".
C'est improbable et drôle.

Enfin Alexandre Forest convoque, dans une autre version du numéro, la méchanceté gratuite, ou oser dire ce que personne n'oserait dire.
Alors qu'il défend les transgenres, Alexandre Forest s'attaque aux personnes qui travaillent comme serveurs à l'auto : "si y'avait pas des personnes laides, qui nous serviraient au service à l'auto".
Ce procédé est intéressant car il permet à l'humoriste de montrer qu'il peut être féroce.
Il déplace l'attention vers une catégorie de personnes définie non pas d'après leur genre ou leur sexe, mais leur métier.

## Remarques générales
Sous la vidéo de ce numéro il y a un commentaire, "C’est pas un sketch c’est un pamphlet.", et il est révélateur d'une situation du stand-up.
Ce que sous-entend probablement ce commentaire, c'est le caractère politique du numéro.
Comme si, parce que le sujet est politique et l'approche très argumentative, ce n'était pas de l'humour ou du stand-up, mais un pamphlet – je surinterprète volontairement ce commentaire.
Pourtant, si l'on utilise le même type d'argumentation qu'Alexandre Forest, ce n'est pas parce qu'un numéro de stand-up ne parle que de voiture qu'on va dire que c'est une publicité pour ce moyen de transport.
Et ce n'est pas non plus parce qu'un·e humoriste va aborder la question de la légalisation du pot que l'on va dire qu'il s'agit d'une tribune.
Alexandre Forest, sans pouvoir être généralisé, est toutefois représentatif d'une génération d'humoristes qui cassent les codes établis, comme Fred Dubé, Rosalie Vaillancourt ou Colin Boudrias.
Si elles et ils pourraient être jugés comme étant en marge, ils souhaitent plutôt construire un monde plus représentatif d'une réalité étouffée :

> Soyez qui vous voulez être, pi empêchez-vous pas de jouer au volley-ball !

## Pour en savoir plus

### Quelques autres créations d'Alexandre Forest

- Un autre numéro lors de l'Open Mic du 18 février 2019, sur la thématique des problèmes psychologiques : [https://noovo.ca/videos/lopen-mic-de/le-stand-up-complet-dalexandre-forest](https://noovo.ca/videos/lopen-mic-de/le-stand-up-complet-dalexandre-forest)
- Le balado En chair et en ondes avec notamment la série "Tousse dans ton coude" : [https://smarturl.it/TousseDansMonCoude](https://smarturl.it/TousseDansMonCoude)
- Sur les réseaux sociaux son compte Instagram ([https://www.instagram.com/alexandre0forest/](https://www.instagram.com/alexandre0forest/)) et sa page Facebook ([https://www.facebook.com/Alexandre0forest/](https://www.facebook.com/Alexandre0forest/))

### Bibliographie succincte

Joubert, Lucie. 2010. « Rire : le propre de l’homme, le sale de la femme ». Dans _Je pense donc je ris. Humour et Philosophie_, 85‑101. Laval: Presses de l’Université de Laval.
