---
title: "Ding et Dong"
date: 2020-05-06
author: "Julien Béland-Bonenfant"
youtube_id: SJZOobJRpUM
duree: 106-450
description: Un humour irrévérencieux, qui se permet tout et qui n'épargne personne.
draft: true
portrait: /img/artistes/ding-et-dong.svg
---
## Présentation du duo

Le duo humoristique Ding et Dong est formé de Claude Meunier et de Serge Thériault.

<span class="lettrine">C</span>laude Meunier est un humoriste, acteur, écrivain et dramaturge québécois. Il a fait ses études en droit avant de se tourner vers les arts de la scène. Sa carrière commence au théâtre, où il est surtout connu pour avoir co-écrit *Broue* en 1979. L'année suivante, il écrit la pièce *Les voisins*, dont il est de la distribution. C'est également le cas pour la très célèbre série télévisée *La petite vie*, qui sera diffusée dans les années 1990. Il réalise son premier film *Le grand départ* en 2008. Il crée aussi la série *Adam et Ève* en 2012, qui n'aura pas le succès escompté auprès du public. Au fil de sa carrière, il a également écrit quelques romans et bandes-dessinées. De son côté, Serge Thériault est un humoriste, scénariste et acteur québécois. Il est notamment connu pour avoir fait partie des groupes humoristiques Paul et Paul et évidemment Ding et Dong. Au petit et au grand écran, il fait partie de la distribution de *La petite vie* et de la série de films *Les Boys*.

Ils forment à eux deux le duo humoristique québécois Ding et Dong, qui évolue principalement dans les années 1980. C'est par le groupe humoristique Paul et Paul, qui comptait auparavant Jacques Grisé, qu'ils se font connaître du public. Le trio a été en activité de 1976 à 1981, avant que Claude Meunier et Serge Thériault n'y mettent fin. Au début des années 1980, les deux humoristes font un voyage en Californie, où ils découvrent le stand-up américain, qui consiste à raconter devant public des histoires dont les blagues s'enchaînent les unes à la suite des autres sans lien logique évident. À leur retour, ils décident d'adapter le stand-up de Los Angeles à Montréal et créent la soirée des Lundis des Ha! Ha! au Club Soda en 1983, où ils jouent le rôle des maîtres de cérémonie. Le succès est tel qu'ils lancent leur première tournée dès l'année suivante, en 1984. En 1990, ils sortent *Ding et Dong, le film*. Ils montent ensuite le spectacle *Le monde merveilleux de Ding et Dong* en 1992. Cette même année marque également la fin du duo.

## Présentation du numéro

L'adaptation que Ding et Dong font du stand-up américain passe par une théâtralisation totale du spectacle. Il s'agit maintenant d'un stand-up thématique, qui consiste à raconter des blagues sur le même sujet tout au long du numéro. Dans celui du *Pape*, le rire est provoqué par la rencontre absurde de deux mondes opposés, la religion et le vedettariat. Ding et Dong se moquent donc à la fois d'un sujet sérieux et d'un sujet frivole, ce qui permet de les critiquer l'un et l'autre. Ils mettent de l'avant tous les travers de l'Église et du vedettariat dans une moquerie pure et simple, qui tient un peu du carnavalesque. Les systèmes de pouvoir sont renversés et reconfigurés afin d'en exacerber les abus. Les deux humoristes pratiquent un humour libérateur qui permet de rire de tous les sujets, sans retenue et sans honte.

{{< bloc >}}
Le pape fait maintenant des « gros hits », a un « gros chalet à Rome », et va dans des « partys ».
{{< /bloc >}}

## Analyse du numéro

Les deux humoristes, lorsqu'ils quittent leur rôle de maîtres de cérémonie, personnifient d'autres personnages à l'aide notamment de la stratégie fictionnelle du faux invité. Le costume du pape, incarné par Claude Meunier (Serge Thériault joue l'intervieweur), est volontairement mal fait, avec une petite plaque sur la tête, une soutane blanche et une veste pare-balles rouge. Les rires du personnage, sa confiance surdimensionnée, ses adresses au public et sa gestuelle décontractée rappellent l'attitude qu'aurait une vedette populaire devant ses fans.

Le numéro repose sur la parodie. Quand il arrive sur scène, le pape embrasse son micro, un geste qui ridiculise ce que faisait vraiment le pape avec les mains de ses fidèles. Il ne s'agit pas de donner une imitation véridique du pape Jean-Paul II. Les blagues, par exemple, ne relèvent pas de ses réels agissements. L'imitation qui est faite ici est une création qui répond à un seul fantasme : s'asseoir devant le pape et rire de lui. Cela se rapproche d'un humour cathartique, dans la mesure où on donne la chance au public de vivre une situation qu'il ne pourrait pas vivre autrement.

Toutes ces composantes scéniques, qui mettent à profit la gestuelle, la posture, les costumes et les dialogues, sont mises au service du propos, qui est ici la critique par le grossissement parodique de la religion. Le numéro s'attaque à la religion par la figure du pouvoir par excellence, le pape. Les deux humoristes se moquent notamment de sa énorme richesse, de son supposé savoir « infaillible », de sa faible estime des femmes et de l'homosexualité au sein de l'Église.

{{< bloc >}}
Le procédé consiste à combiner la métaphore à une syllepse, c'est-à-dire à utiliser un mot au sens propre et au sens figuré en même temps, comme dans « embrasser le sol d'un pays » et « embrasser le train d'un monseigneur ».
{{< /bloc >}}

Il est important de mentionner que dans le contexte des années 1980, la religion ne commande plus le respect d'autrefois. L'Église, qui a longtemps fixé la morale et les interdits sociaux, est en perte de vitesse, les gens ne voulant plus entendre parler de religion, comme le d'ailleurs laisse entendre l'une des dernières blagues du numéro: « EP go home, EP go home ». Si Ding et Dong se permettent de nommer explicitement le pape Jean-Paul II, c'est bien parce que l'Église a perdu de son importance pour les gens. Pour beaucoup, la religion est surtout symbolique mais elle reste une institution autoritaire. Se moquer d'un tel pouvoir est donc libérateur. S'il y a dans ce numéro une certaine critique, le but de Ding et Dong n'est cependant pas de lancer un message politique. Les abus de l'Église sont dénoncés dans un grand éclat de rire. Il s'agit surtout de tourner en dérision un sujet qui est d'habitude sérieux.

Sous le couvert de la religion, Ding et Dong en profitent pour s'en prendre à une autre cible : le vedettariat. La parodisation du pape passe effectivement par sa transformation en vedette. On lui pose des questions comme on le ferait avec un chanteur ou un acteur populaire. Le pape fait maintenant des « gros hits », a un « gros chalet à Rome », et va dans des « partys ». Les activités du pape sont ridiculisées en étant rabaissées à un niveau trivial. Ding et Dong se moquent de la religion, mais ils critiquent aussi le mode de vie des artistes internationaux que les gens idéalisent et envient.

L'humour de Ding et Dong est avant toute chose un humour qui passe par la parole. Le travail sur la langue met de l'avant l'absurdité et la perte de sens du langage. Un des procédés comiques que le duo utilise fréquemment est la métaphore réactivée. Le procédé consiste à combiner la métaphore à une syllepse, c'est-à-dire à utiliser un mot au sens propre et au sens figuré en même temps, comme dans « embrasser le sol d'un pays » et « embrasser le train d'un monseigneur ». Beaucoup de répliques contiennent des calembours : le Vatican transformé en « fatiguant », le film de « pape ou d'épée » ou encore l'antanaclase, causée cette fois-ci par la récurrence d'un même mot dans deux sens différents : « sauve toi pas, faut que je te sauve ». Il y a également la reconfiguration de discours convenus, notamment dans les phrases vides du présentateur et le nom du talkshow : « Entre deux chaises », qui renvoie volontairement à un sentiment d'inconfort. L'expression qui revient tout au long du numéro est l'interjection « kin toi! », qui, par sa simple utilisation, déclenche le rire. Le public s'esclaffe lorsqu'il reconnaît cette expression, à la manière d'un *running gag*.

Cette surabondance de blagues et le débit rapide des humoristes ne laissent aucun répit au public, qui n'a d'autre choix que de plonger dans l'univers du duo. En effet, Ding et Dong créent un monde fantasmé où la parole désincarnée est un véritable exutoire. C'est un monde coupé de la société, de ses mœurs et de sa censure, un monde coloré où le langage procède à la déconstruction de la réalité.

## Conclusion

Ce numéro contient plusieurs éléments que l'on peut retrouver dans la majorité des numéros de Ding et Dong, notamment le recours à un personnage caricaturé, un travail élaboré de mise en scène, l'attaque faite à une autorité et une grande variété de jeux langagiers. C'est un humour irrévérencieux, qui se permet tout et qui n'épargne personne. Il n'a pas de limites pour provoquer le rire. En fin de compte, l'humour de Ding et Dong s'en prend à tous les systèmes de pouvoir qui contraignent l'imaginaire social et qui balisent le regard critique du public.

## Pour en savoir plus

### Études sur Ding et Dong

{{< references >}}
Aird, R. (2004). *L'histoire de l'humour au Québec. De 1945 à nos jours.* Montréal : VLB Éditeur.

Deglise, F. (2010, 17 juillet). *Claude Meunier : un Molière asocial et grognon*. Le Devoir. https://www.ledevoir.com/culture/292719/claude-meunier-un-moliere-asocial-et-grognon

Deglise, F. (2012, 1 décembre). *Le Club Soda a 30 ans -- Claude Meunier se souvient des Lundis... un jeudi*. https://www.ledevoir.com/culture/theatre/365198/claude-meunier-se-souvient-des-lundis-un-jeudi
{{< /references >}}