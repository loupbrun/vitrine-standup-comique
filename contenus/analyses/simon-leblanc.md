---
title: "Simon Leblanc"
date: 2020-05-06
author: "Sandrine Comeau"
youtube_id: SJZOobJRpUM
description: "Un talent de conteur et un humour intelligent et bien mené."
draft: true
portrait: /img/artistes/simon-leblanc.svg
---
## Présentation de l'artiste

<span class="lettrine">C</span>'est un soir d'octobre 2009 que Simon Leblanc participe pour la première fois à un micro ouvert du Pub montréalais Le Saint-Ciboire. Étudiant au baccalauréat en psychologie de la communication et fervent amateur d'improvisation, il ne peut imaginer l'impact qu'aura cette soirée d'humour sur ses plans professionnels. Le sentiment qu'il éprouve sur les planches est indescriptible et addictif, l'incitant même à abandonner ses cours universitaires.

C'est avec sa femme comme gérante qu'il se lance tête première dans le grand monde de l'humour québécois. Il participe avec enthousiasme à l'émission *En route vers mon premier Gala Juste pour rire 2010* qui le propulse dans son ascension. Il entreprend ensuite une tournée de deux ans pour le spectacle *Union Libre* avec François Bellefeuille et collabore à de nombreuses émissions télévisées comme *Juste pour rire en direct* (TVA), *Selon l'opinion comique* (MATV) et *Brassard en direct d'aujourd'hui* (VTÉLÉ)[^1].

{{< bloc >}}
Le talent de conteur de Simon Leblanc le fait briller dans l'industrie et comme le clament les critiques, cette étoile montante n'est pas près de s'éteindre.
{{< /bloc >}}

Simon Leblanc, de nature très humble, ne cesse de répéter qu'il exerce ce métier par amour pour le public et que la popularité ne figure pas sur sa liste de souhaits. C'est bien malgré lui qu'il reçoit toutes ces critiques positives et ces multiples honneurs. Il remporte, entre autres, le prix *Révélation de l'année au Festival Juste pour rire* en 2013, *l'Olivier de* la *Découverte de l'année en 2014*, ainsi que *l'Olivier de l'auteur de l'année* en 2017 qu'il partage fièrement avec Olivier Thivierge, auteur avec qui il travaille depuis maintenant neuf ans.

Mais qui mentionne-t-il dans ses discours ? Sa femme, sa famille, ses amis, son chat, oui, sûrement. Cependant, contrairement à de nombreux collègues de l'industrie, le jeune homme ne remercie pas l'École nationale de l'humour pour toutes les sessions de stimulation intellectuelle. C'est, en effet, sans la populaire institution, par essais et erreurs dans les soirées d'humour, qu'il a appris son métier.

Cet autodidacte originaire de Gaspésie s'inspire entre autres de son expérience de changement de ville, ses problèmes de santé et ses tranches de vie pour créer des numéros plus hilarants les uns que les autres. Il ne peut nier ses racines : les traditions gaspésiennes ont grandement contribué à former son talent de conteur.

Après ses deux premiers spectacles *Tout court* et *Malade,* il est actuellement en tournée pour son troisième one-man-show *Déjà*, dont les représentations ont été reportées à cause de la pandémie.

## Présentation du numéro

Issu du Gala *Grand rire de Québec 2013*, son numéro *La pêche* représente une des nombreuses facettes du talent de l'humoriste. En commençant par une anecdote sur la congestion routière montréalaise à laquelle chacun est en mesure de s'identifier, il nous fait voyager avec lui à la campagne pour nous raconter les mille et une péripéties d'une journée de pêche avec son beau-frère. C'est sans effets spéciaux ni feux d'artifice que Simon Leblanc, grâce à la clarté de ses propos et sa capacité à nous dérouter, réussit à provoquer un rire sincère.

## Analyse du numéro

### La performance

Avant de nous pencher sur le contenu du texte, il me semble pertinent d'analyser la façon dont l'artiste livre la marchandise. Premièrement, il est évident qu'il mise énormément sur le comique de geste. Son piétinement nerveux, le manque de fluidité dans ses mouvements et ses expressions faciales inusitées contribuent largement à créer l'hilarité du public.

Il est connu que les comiques doivent opter pour un langage familier et accessible. Des mots trop compliqués ou des formulations littéraires risquent de faire tomber les blagues à plat. Simon Leblanc ne prend pas cette question à la légère : son parler et son accent gaspésien sont loin de passer inaperçus. Même si le contenu n'est pas préalablement drôle, les phrases comme « Donne-moi un *ratchet,* je vais le d*ebolter*, mon char, quessé tu veux que je fasse ? » {{< temp 100 >}} ne peuvent nous laisser indifférents.

J'aimerais aussi souligner que Simon Leblanc donne souvent l'impression de perdre le fil de ses idées en riant de ses propres blagues. Il assure en entrevue que ces moments sont improvisés, mais reste qu'ils servent principalement à révéler la vulnérabilité et l'humanité de l'artiste et à renforcer le « sentiment de connivence existant entre le performeur et le public »[^2].

{{< bloc >}}
Son piétinement nerveux, le manque de fluidité dans ses mouvements et ses expressions faciales inusitées contribuent largement à créer l’hilarité du public.
{{< /bloc >}}

### La mécanique

Le texte prend la forme du monologue et contient de nombreux punchs dignes des grands stand-up comiques. Tout au long de son anecdote, il adopte un point de vue autodérisoire ; il ne se ménage pas, tant dans ses propos que dans ses imitations.

L'élaboration du numéro peut sembler simple, mais derrière chaque rire se cache une mécanique impeccable. Les punchs sont construits selon des principes bien connus en humour.

En effet, chacun d'eux contient une prémisse, soit « la rampe de lancement, la mise en place des éléments nécessaires à la compréhension »[^3], qui est suivie d'une ou plusieurs chutes, « l'aboutissement de la blague, là où les gens réagissent »[^4].

Je me pencherai sur un exemple tiré de *La pêche* pour mieux illustrer cela. Simon Leblanc raconte son aventure dans le lac. Dans une prémisse courte et efficace {{< temp 210 >}}, il explique que son beau-frère et lui accrochent le moteur sur le côté de l'embarcation. Dans les premières secondes, il assure que tout se passe comme prévu et qu'ils ont beaucoup de plaisir, et ce même si le canot « ne trippe pas ». Cette introduction nous conduit directement à plusieurs chutes. La première est induite par une personnification du bateau lorsqu'il s'arrête d'un coup : « J'ai jamais vu un canot se comporter comme ça. Jamais ». Cette manière de voir la situation est surprenante puisqu'on s'attend à une petite panique chez les personnages. Par la suite, il accumule les chutes, en enchaînant une référence à une légende importante de la culture québécoise (« La chasse-galerie ! »), un jeu de mots (« C'était plus de la navigation, c'était de l'aviation »), ainsi qu'une comparaison en rapport avec la pêche qui n'a pourtant jamais lieu (« On remontait le courant comme un saumon qui fraye, c'était magique »). Ces éléments créent une surprise, puisqu'ils sortent de nulle part et vont à contre-courant du danger qui les guette. Cette façon de voir la situation rassure le public, elle lui permet de s'esclaffer sans s'inquiéter des conséquences, rappelant l'importance de la notion d'innocuité[^5] en humour. Si on se fie aux réactions de l'auditoire, chaque chute amplifie le rire déjà bien installé.

Bien que l'humoriste assure qu'il ne couche pas ses textes sur papier, on voit bien que derrière chaque punch se cache une préparation chirurgicale. Pour provoquer une réaction, les artistes travaillent des heures pour trouver le bon mot, le bon rythme et la bonne mimique. De toute évidence, cela vaut aussi pour Simon Leblanc.

## Conclusion

Un des éléments clés du succès de Simon Leblanc est son don pour créer des images précises qui ont le don de nous surprendre. Je trouve important de relever qu'il réussit à soulever les foules sans tenir de propos discriminatoires ou dénigrants envers certains groupes ou personnes marginalisés. Mise à part une blague inoffensive sur les ingénieurs, en aucun cas il n'utilise la méchanceté gratuite. Son humour est intelligent et bien mené. Le talent de conteur de Simon Leblanc le fait briller dans l'industrie et comme le clament les critiques, cette étoile montante n'est pas près de s'éteindre.

## Pour en savoir plus

### Bibliographie

{{< references >}}
EMELINA, Jean , *Le comique. Essai d'interprétation générale*, Paris, SEDES, « Présences critiques » 1991, 211 p.

LARRUE, Jean-Marc, « Composantes et procédés du stand-up », Montréal : Université de Montréal, session hiver 2020.

LECLERC, Yves, « Simon Leblanc, un vrai passionné », *Le journal de Québec*, 13 novembre 2018, <https://www.journaldequebec.com/2018/11/13/simon-leblanc-un-vrai-passionne>

OUELLET, Sylvie et Christiane VIEN, *Écrire l'humour, c'est pas des farces*, Montréal, Éditions Druides, 2017, 240 p.

Radio-Canada, « Simon Leblanc : un conteur gaspésien parmi les humoristes », sur le site de Radio-Canada, 5 novembre 2016, <https://ici.radio-canada.ca/nouvelle/813014/simon-leblanc-spectacle-amqui-matane>

R.ROY, Marie-Josée, « «Malade» de Simon Leblanc : en bas de la ceinture », *Le journal de Montréal*, 22 janvier 2019, <https://www.journaldemontreal.com/2019/01/22/malade-de-simon-leblanc-en-bas-de-la-ceinture>

TARDIF, Dominique, « Pourquoi Simon Leblanc rit-il de toutes ses blagues ? », *Le Devoir*, 21 janvier 2019, <https://www.ledevoir.com/culture/theatre/545963/pourquoi-simon-leblanc-rit-il-de-toutes-ses-blagues>

Sans auteur. « Simon Leblanc », sur le site de *Phaneuf*. Consulté le 20 mars 2020. <https://phaneuf.ca/artistes/simon-leblanc/>

Sans auteur, « Simon Leblanc », sur le site SPEC du Haut Richelieu. Consulté le 25 mars 2020. <https://spec.qc.ca/artiste/simon-leblanc>
{{< /references >}}

[^1]: Sans auteur, « Simon Leblanc », sur le site SPEC du Haut Richelieu. Consulté le 25 mars 2020. <https://spec.qc.ca/artiste/simon-leblanc>

[^2]: LARRUE, Jean-Marc Larrue, « Composantes et procédés du stand-up », Montréal : Université de Montréal, session hiver 2020.

[^3]: OUELLET, Sylvie et Christiane VIEN, *Écrire l'humour, c'est pas des farces*, Montréal, Éditions Druides, 2017, p.41

[^4]: OUELLET, Sylvie et Christiane VIEN, *Écrire l'humour, c'est pas des farces*, Montréal, Éditions Druides, 2017, p.41

[^5]:  Cette idée selon laquelle le public ne doit pas se sentir en danger en humour (voir Jean EMELINA, *Le comique. Essai d'interprétation générale*, Paris, SEDES, « Présences critiques » 1991)
