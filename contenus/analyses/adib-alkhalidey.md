---
title: "Adib Alkhalidey"
date: 2020-05-06
author: "Raphaël Boudreau-Pineault"
youtube_id: 02nzzgJVU5k
draft: true
description: "Adib Alkhalidey parvient à allier subtilement humour et critique sociale."
portrait: /img/artistes/adib-alkhalidey.svg
---
## Présentation de l'artiste
<span class="lettrine">A</span>dib Alkhalidey est né en 1988 d'une mère marocaine et d'un père irakien. Diplômé de l'École nationale de l'humour en 2010, il est très vite reconnu comme un humoriste de talent (prix du jury au Festival d\'humour franco-québécois de Lourdes, 2012 ; prix de la révélation de l'année au Gala Les Oliviers, 2013). Traversés par une inquiétude face à la dureté des rapports humains, ses deux premiers one-man-shows (*Je t'aime*, 2013 ; *Ingénu*, 2017) insistent avec humour sur l'importance de la tolérance et de l'empathie pour qu'une société puisse « vivre ensemble ». Plus récemment, Adib Alkhalidey a annoncé la représentation de son troisième one-man-show (*Apatride*, 2019). Il est l'un des quatre humoristes québécois à avoir présenté un numéro pour la série *Humoristes du monde* (*Comedians of the World*) produite par Netflix.

Adib Alkhalidey s'illustre aussi en tant que comédien, dans des séries télévisées (*Like-moi !* ; *Code G*), des webséries (*7\$*) et au grand écran (*Matthias et Maxime* de Xavier Dolan). Passionné de cinéma, il est l'auteur d'un court-métrage (*Va jouer dehors*, 2017) et d'un film mettant en vedette Julien Lacroix (*Mon ami Walid*, 2019).

{{< bloc >}}
Son talent à employer les dispositifs comiques pour mettre en lumière l’absurdité des comportements humains, font de son humour un mélange original de critique et de moquerie.
{{< /bloc >}}

Le côté quelque peu névrosé du personnage à travers lequel Adib Alkhalidey se met en scène ne parvient jamais à éclipser la personnalité sensible et un peu vulnérable qu'il met en avant dans ses spectacles. Son humour proprement dit se distingue par sa volonté de « dire quelque chose[^1] », d'amener à réfléchir sans pour autant se faire moralisateur. Tout en pointant du doigt la haine et l'intolérance qui minent les rapports sociaux (en raillant par exemple les comportements racistes, homophobes ou sexistes), l'humoriste insiste non seulement sur la nécessité du respect, mais aussi sur le rôle du rire à l'intérieur d'une société qui aspire à être « solidaire ». Le souci qu'il accorde à l'écriture du texte comique se reflète dans la langue soignée qu'il emploie sur scène et dans la finesse de construction de certains numéros. L'art de la digression qu'il met en œuvre avec une maîtrise rare, sa capacité à pousser certains raisonnements jusqu'à une extrémité quelquefois délirante, et son talent à employer les dispositifs comiques pour mettre en lumière l'absurdité des comportements humains, font de son humour un mélange original de critique et de moquerie.

## Présentation du numéro
« Le parc à chien », d'une durée d'environ sept minutes, est un extrait d'*Ingénu*, le deuxième one-man-show d'Adib Alkhalidey. Dans ce numéro, l'humoriste raconte sur un ton faussement candide une anecdote prétendument tirée de sa vie personnelle. Après avoir « acheté un chien » à un « gars de Kijiji », il retrace sa rencontre avec un propriétaire de pitbull dans un parc à chiens. L'anecdote, qui s'apparente à une fable, est l'occasion de mettre en scène des questions chères à Adib Alkhalidey : l'ouverture face à la différence, les dangers des préjugés et des stéréotypes racistes, l'identité.

## Analyse du numéro

### Remarques générales sur l'espace
Le numéro est performé sur la scène étroite du Bordel Comédie Club. Outre le décor habituel de cette scène (le logo, le mur de fond en briques), on note la présence d'une douzaine de lampes roses suspendues. L'arcade métallique encadrant l'espace scénique est rehaussée par une lumière, rose aussi, qui en épouse le contour.

Cette prédominance du rose trouve écho dans une anecdote du spectacle, dans laquelle Adib Alkhalidey raconte comment son enseignante de morale aurait mis un terme aux querelles racistes qui avaient cours à l'école multiethnique qu'il fréquentait enfant. L'enseignante aurait crûment affirmé que, en supposant qu'elle éventre les querelleurs et qu'elle étale leurs organes sous leurs yeux, ils verraient bien, malgré leurs différences apparentes, qu'ils étaient tous roses à l'intérieur ! S'« ouvrir » à l'autre malgré les différences : c'est là une idée qui traverse tout *Ingénu*, et que l'on retrouve aussi dans « Le parc à chien ».

### Thématique
Par les sujets qu'il met en scène, ce numéro est significatif de l'humour d'Adib Alkhalidey ; l'humoriste y aborde des questions de société qui reviennent à plusieurs reprises dans ses spectacles. À travers la figure de la chienne Chloé, il met en garde contre les dangers de l'intolérance et des préjugés racistes, qui proviennent autant des médias qui les alimentent que du refus de s'ouvrir à la différence. Non sans (auto)dérision, Adib Alkhalidey met aussi en cause, en partant de sa propre expérience, l'idée conventionnelle d'« intégration ». Enfin, il proclame l'urgence du respect et de l'ouverture à l'autre, malgré les différences qui peuvent exister au sein d'une société : il faut se « donner l'occasion de socialiser », sinon « ça va être le chaos ».

### Structure
Sous les apparences de naturel et de spontanéité propres au stand-up comique, le numéro répond à une structure extrêmement réfléchie. Il se découpe en trois mouvements distincts. L'humoriste souligne d'abord le bien-fondé de l'injonction de respect qu'on enseignait dans les cours de morale et de catéchèse, tout en prônant ce qu'il appelle l'« enthousiasme ». Le deuxième moment, qui est le plus long du numéro, est constitué par le récit du chien ; il se divise lui-même en trois parties : d'abord, les raisons expliquant pourquoi l'humoriste a voulu « acheter » un chien ; ensuite, la rencontre avec le « gars de Kijiji » ; enfin, la sortie au parc à chiens. Dans le dernier temps du numéro Adib Alkhalidey réitère l'importance de l'« enthousiasme » et du respect mutuel au sein d'une société. Mi-sérieux, mi-blagueur, il propose aussi sa solution de créer, sur l'exemple du parc à chiens, un « parc à arabes » : il faut que « les gens qui ont peur des Arabes » puissent rencontrer les Arabes « qui leur feront jamais mal ».

Ainsi déplié, le numéro s'apparente presque à une composition rhétorique. Un problème général est exposé d'entrée de jeu : le respect mutuel n'est possible que s'il existe une volonté d'aller à la rencontre de l'autre. Le récit du chien, à l'image d'un apologue, illustre concrètement cette idée abstraite. Enfin, en bon rhéteur, l'humoriste rappelle le problème initialement exposé et propose une solution --- un plan d'action, pourrait-on dire. Naturellement, une telle construction garantit la clarté du propos et concourt à l'efficacité de sa transmission : si l'humour d'Adib Alkhalidey, selon son aveu propre, cherche à « dire quelque chose », il cherche aussi une manière de *bien* le dire. Mais dans les faits, le spectateur n'a pas l'impression d'avoir affaire à une construction aussi rigoureuse : par le recours à l'anecdote et par les différents dispositifs comiques qu'il mobilise, Adib Alkhalidey, adroitement et l'air de rien, conduit le spectateur d'un bout à l'autre du numéro. Il réussit le tour de force d'aborder un problème sociétal délicat et sérieux avec beaucoup de légèreté, sans se montrer moralisateur.

{{< bloc >}}
Il faut que les gens qui ont peur des Arabes puissent rencontrer les Arabes qui leur feront jamais mal.
{{< /bloc >}}

## Procédés
Quels dispositifs Adib Alkhalidey mobilise-t-il pour maintenir l'attention du spectateur et conférer à son propos une impression de légèreté ? À tous points de vue, le jeu de l'humoriste cherche à dynamiser l'histoire qu'il raconte. Sa gestuelle très expressive vise à illustrer le propos en l'animant, en lui donnant vie ; le ton de sa voix, tantôt posé, tantôt criard, jamais monocorde, permet de conserver l'attention du spectateur ; la mise en théâtre du récit, assurée par le recours à des onomatopées et à des dialogues directs, contribue au dynamisme et à l'allègement du sérieux du discours.

L'humoriste fait aussi appel à l'ironie (en proposant par exemple au public de reconduire le stéréotype selon lequel les Arabes ont peur des chiens) et à l'autodérision (en prétendant vouloir se conformer encore davantage, dans une sorte de surenchère ridicule, à l'image d'« exemple d'intégration » qu'un article de journal lui a accolée). Outre leur effet comique, de tels procédés révèlent le côté risible et absurde de certaines idées reçues et invitent le spectateur à les mettre en cause.

Enfin, la chute du numéro prend le spectateur à court et suscite l'hilarité générale grâce à la comparaison saugrenue entre le parc à chiens et l'idée d'un « parc à arabes ». Se proclamant, avec un sérieux feint, « humoriste de solution », Adib Alkhalidey ne laisse rien présager de cette « solution » absurde et baroque : et pourtant, rétrospectivement, tout le récit qui vient d'être raconté semble y avoir conduit, redoublant la surprise et l'hilarité du public. Par-delà les rires qu'elle provoque, l'idée apparemment fantasque de créer un « parc à arabes » sur l'exemple du parc à chiens met en avant la nécessité de favoriser concrètement le contact des différences à l'intérieur d'une société qui aspire à l'empathie et au respect mutuel. Il ne s'agit pas seulement de souhaiter abstraitement la rencontre des différences : il faut lui *donner lieu*.

## Conclusion
« Le parc à chien », exemple remarquable du talent d'Adib Alkhalidey, parvient d'un seul souffle à traiter avec légèreté d'un sujet sérieux, à allier subtilement humour et critique sociale. Si chacun des gags vise naturellement à faire rire, ils sont aussi --- et presque toujours --- objets de réflexion, remises en question. Il ne s'agit pas ici de proclamer la vocation du stand-up en affirmant qu'il devrait à tout prix aborder des enjeux de société. Dans une perspective inverse, on peut néanmoins contempler --- Adib Alkhalidey nous y invite --- tout ce dont peut bénéficier la réflexion sur la société lorsqu'elle ose tirer profit des dispositifs de l'humour --- lorsqu'elle ose abattre la cloison fragile et caduque qui sépare le comique du sérieux, le rire de la pensée.

## Pour en savoir plus
Le site web d'Adib Alkhalidey : <https://www.adibalkhalidey.com/>

GUY, Chantal, « Un nouveau festival d'humour à Montréal », *La Presse*, 1^er^ février 2016. URL : <https://www.lapresse.ca/arts/spectacles-et-theatre/humour-et-varietes/201602/01/01-4946021-un-nouveau-festival-dhumour-a-montreal.php> (page consultée le 20 février 2020).

DUCHARME, André, « L'humoriste Adib Alkhalidey : des cheveux, Martin Matte, un TOC, le français, du slalom... », *L'Actualité*, 8 juin 2013. URL : <https://lactualite.com/culture/lhumoriste-adib-alkhalidey-en-rodage-tout-lete-des-cheveux-martin-matte-des-troubles-obsessifs-compulsifs-le-francais-du-slalom/> (page consultée le 20 mai 2020).

MÉNARD, Elizabeth, « 7 questions à Adib Akhalidey », *Le Journal de Montréal*, 11 juillet 2014. URL : <https://www.journaldemontreal.com/2014/07/11/7-questions-a-adib-alkhalidey> (page consultée le 20 mai 2020).

TARDIF, Dominique, « Rendre le monde moins laid avec Adib », *Le Devoir*, 20 mars 2017. URL : <https://www.ledevoir.com/culture/494345/rendre-le-monde-moins-laid-avec-adib> (page consultée le 20 mai 2020).

[^1]: DUCHARME, André, « L'humoriste Adib Alkhalidey : des cheveux, Martin Matte, un TOC, le français, du slalom... », L'Actualité, 8 juin 2013. URL : https://lactualite.com/culture/lhumoriste-adib-alkhalidey-en-rodage-tout-lete-des-cheveux-martin-matte-des-troubles-obsessifs-compulsifs-le-francais-du-slalom/ (page consultée le 20 mai 2020).
