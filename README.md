# Vitrine sur le Stand-up au Québec

Projet découlant du séminaire sur le Stand-up donné par Jean-Marc Larrue à l’Université de Montréal à l’hiver 2020.


## 🛠  Installation

Le projet requiert [Hugo](https://gohugo.io/).


## 💻 Local

Lancer un serveur local (http://localhost:1313 par défaut):

```bash
hugo serve --disableFastRender
```


## 📦 Production

Construire le site dans le dossier `public/`:

```bash
rm -rf public # faire le ménage du dossier
hugo --minify # construire le site avec minification
```


## 🚀 Déploiement

Lorsque les fichiers sont construits dans le dossier `public/`, il ne reste plus qu’à les copier vers le serveur.
Cela peut être fait de plusieurs manières.

Une solution avec [Vercel Now](https://vercel.com/) a été configurée.
Il s’agit simplement de pousser les changements sur la branche principale `master` et le site sera automatiquement déployé.

http://standup-quebec.now.sh


## Shortcodes

### 📢 bloc

Un gros bloc de texte.

```md
{{< bloc >}}
Un gros bloc de texte mis en évidence.
(De préférence pas trop long, soit 2-5 lignes).
{{< /bloc >}}
```

### 📝 references

Entourer la liste des références pour une mise en forme adéquate.

```md
{{< references >}}
LARRUE, Jean-Marc, « Composantes et procédés du stand-up », Montréal : Université de Montréal, session hiver 2020.

...
{{< /references >}}
```

- Chaque paragraphe représente une entrée;
- La première ligne de chaque entrée ne sera pas en retrait (les suivantes oui);
- Les liens seront coupés (car souvent de longs URL qui débordent)

### ⏩ temp

Pour sauter à un moment de la vidéo YouTube dont l’identifiant est spécifié dans le YAML front-matter (`youtube_id`).

```md
{{< temp 123 >}}
```

L’argument, obligatoire, indique le **temps en secondes** (un format de type `MM:SS` ne fonctionnera pas).

## ⚙️ Développement

ℹ️ Cette section concerne uniquement le développement technique (pas nécessaire lors de la modification des contenus).

### Styles

Pour développer des styles, on peut jouer directement avec `actifs/styles` ([SCSS](https://sass-lang.com/)).
Aucun outil supplémentaire n'est nécessaire.

### TailwindCSS

Une couche d’utilités CSS est générée avec la bibliothèque [TailwindCSS](https://tailwindcss.com/), incluse dans les styles généraux (`actifs/styles/styles.scss`).
La feuille est ensuite optimisée avec [PostCSS](https://postcss.org/) pour n’inclure que les règles nécessaires.
[NodeJS](https://nodejs.org) v10+ est requis.

```bash
npm install  # installation des paquets NodeJS
```

Pour reconstruire le fichier `tailwind.custom.css` (une collection d’utilités CSS, dont les règles non utilisées seront élaguées):

```bash
npm run build:styles  # postcss w/ minification pis toute
```

En mode développement, on évitera d’optimiser le fichier `tailwind.custom.css`:

```bash
npm run serve:styles  # postcss w/ watch
```

## Licence

À discuter selon les droits des étudiants.

[WTFPL](http://wtfpl.net/about/)
